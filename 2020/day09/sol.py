class Solution:
    def __init__(self, data):
        self.data = list(map(int,data.split("\n")[:-1]))

    def answer1(self, step = 25):
        S = set(self.data[:step])
        i = step
        while True:
            val = self.data[i]
            if not is_sum(S, val):
                return val
            S.remove(self.data[i-step])
            S.add(val)
            i += 1

    def answer2(self, step = 25):
        objectiv = self.answer1(step)
        beg = 0
        end = 0
        value = self.data[0]
        while value != objectiv:
            if value < objectiv:
                end += 1
                value += self.data[end]
            if value > objectiv:
                value -= self.data[beg]
                beg += 1
        return min(self.data[beg:end+1]) + max(self.data[beg:end+1])

def is_sum(S, val):
    temp = S.copy()
    while temp:
        i = temp.pop()
        if val - i in temp:
            return True
        
test_data = """35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    print(sol1.answer1(5))
    assert(sol1.answer1(5) == 127)
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print(sol.answer1())

    
    if sol1.answer2(5) == None:
        exit(0)
    print("\n\n\n\t\t PART 2\n\n")
    print(sol1.answer2(5))
    assert(sol1.answer2(5) == 62)
    print("tests 2 Ok !")
    print()
    print(sol.answer2())
