from collections import defaultdict

class Equation:
    def __init__(self, data):
        self.data = data

    def left_to_right_evaluate(self):
        data = self.data.split()
        op = defaultdict(str)
        first = data[0]
        h = first.count('(')
        op[h] = first[h:]
        for w in data[1:]:
            if w in '+*':
                op[h] += w
                continue
            paren_o = w.count('(')
            paren_c = w.count(')')
            h += paren_o
            new_int = w[paren_o:len(w)-paren_c]
            op[h] = str(eval(op[h] + new_int))
            for i in range(paren_c):
                h -= 1
                op[h] = str(eval(op[h] + op[h+1]))
                op[h+1] = ''
        return int(op[0])

    def add_first_evalutate(self):
        data = self.data.split()
        for i, w in enumerate(data):
            if w == '+':
                h = 0
                k = i
                while True:
                    k -= 1
                    if data[k] in '+*':
                        continue
                    h += data[k].count(')')
                    h -= data[k].count('(')
                    if h <= 0:
                        data[k] = '(' + data[k]
                        break
                h = 0
                k = i
                while True:
                    k += 1
                    if data[k] in '+*':
                        continue
                    h -= data[k].count('(')
                    h += data[k].count(')')
                    if h >= 0:
                        data[k] = data[k] + ')'
                        break
        return eval(' '.join(data))
            

class Solution:
    def __init__(self, data):
        self.data = list(map(Equation,data.split("\n")[:-1]))

    def answer1(self):
        res = 0
        for eq in self.data:
            res += eq.left_to_right_evaluate()
        return res

    def answer2(self):
        res = 0
        for eq in self.data:
            res += eq.add_first_evalutate()
        return res
        
test_data = """1 + 2 * 3 + 4 * 5 + 6
1 + (2 * 3) + (4 * (5 + 6))
2 * 3 + (4 * 5)
5 + (8 * 3 + 9 + 3 * 4 * 3)
5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))
((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    print(sol1.answer1())
    assert(sol1.answer1() == 71+51+26+437+12240+13632)
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print(sol.answer1())

    
    if sol1.answer2() == None:
        exit(0)
    print("\n\n\n\t\t PART 2\n\n")
    print(sol1.answer2())
    assert(sol1.answer2() == 231+51+46+1445+669060+23340)
    print("tests 2 Ok !")
    print()
    print(sol.answer2())
