class Mask:
    def __init__(self, info):
        self.zeros = set()
        self.ones = set()
        self.exes = set()
        for i, val in enumerate(info[::-1]):
            if val == '1':
                self.ones.add(i)
            elif val == '0':
                self.zeros.add(i)
            else:
                self.exes.add(i)

    def appl(self, val):
        for i in self.ones:
            val = val | (1<<i)
        for i in self.zeros:
            val = val & ~(1<<i)
        return val

    def apply_mem(self, val):
        for i in self.ones:
            val = val | (1<<i)
        possib = {val}
        for i in self.exes:
            temp = set()
            for vv in possib:
                temp.add(vv | (1<<i))
                temp.add(vv & ~(1<<i))
            possib = temp
        return possib

class Solution:
    def __init__(self, data):
        self.data = data.split("\n")[:-1]

    def answer1(self):
        mask = None
        mem = {}
        for row in self.data:
            op, _, val = row.split()
            if op == 'mask':
                mask = Mask(val)
            else:
                mem_val = op[4:-1]
                mem[mem_val] = mask.appl(int(val))
        return sum(mem.values())
                

    def answer2(self):
        mask = None
        mem = {}
        for row in self.data:
            op, _, val = row.split()
            if op == 'mask':
                mask = Mask(val)
            else:
                mem_val = int(op[4:-1])
                for mem_add in mask.apply_mem(mem_val):
                    mem[mem_add] = int(val)
        return sum(mem.values())
        
        
test_data = """mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
mem[8] = 11
mem[7] = 101
mem[8] = 0
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    print(sol1.answer1())
    assert(sol1.answer1() == 165)
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print(sol.answer1())

    test_data = """mask = 000000000000000000000000000000X1001X
mem[42] = 100
mask = 00000000000000000000000000000000X0XX
mem[26] = 1
"""
    sol1 = Solution(test_data)
    
    if sol1.answer2() == None:
        exit(0)
    print("\n\n\n\t\t PART 2\n\n")
    print(sol1.answer2())
    assert(sol1.answer2() == 208)
    print("tests 2 Ok !")
    print()
    print(sol.answer2())
