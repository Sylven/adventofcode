class Solution:
    def __init__(self, data):
        self.data = data.split("\n")[:-1]

    def answer1(self):
        self.seats = [0] * len(self.data)
        for i, ticket in enumerate(self.data):
            row, col = ticket[:7], ticket[7:]
            row = int(row.replace('B', '1').replace('F', '0'), 2)
            col = int(col.replace('R', '1').replace('L', '0'), 2)
            self.seats[i] = (row, col)
        return max(8 * row + col for (row, col) in self.seats)

    def answer2(self):
        plane = [['.' for _ in range(8)] for _ in range(128)]
        for row, col in self.seats:
            plane[row][col] = 'x'

        for i, row in enumerate(plane):
            print(''.join(row) + '  ', i)
        
test_data = """BFFFBBFRRR
FFFBBBFRRR
BBFFBBFRLL
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    print(sol1.answer1())
    assert(sol1.answer1() == 820)
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print(sol.answer1())

    
    
    print("\n\n\n\t\t PART 2\n\n")
    print("tests 2 Ok !")
    print()
    print(sol.answer2())
    print(87 * 8 + 3)
