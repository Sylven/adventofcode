class Solution:
    def __init__(self, data):
        self.data = list(map(int,data.split("\n")[:-1]))

    def answer1(self):
        temp = sorted(self.data)
        diff = {1:0, 2:0, 3:1}
        prev = 0
        for val in temp:
            diff[val-prev] += 1
            prev = val
        return diff[1] * diff[3]

    def answer2(self):
        temp = tuple(sorted(self.data))
        done = {}
        return sol2(0, max(self.data) + 3, temp, done)

def sol2(mini, maxi, values, done):
    if (mini, values) in done:
        return done[(mini, values)]
    if not values:
        return maxi - mini <= 3
    if values[0] - mini > 3:
        return 0
    new_val = values[1:]
    done[(mini, values)] = sol2(mini, maxi, new_val, done) + sol2(values[0], maxi, new_val, done)
    return done[(mini, values)]
    
    
        
test_data = """28
33
18
42
31
14
46
20
48
47
24
23
49
45
19
38
39
11
1
32
25
35
8
17
7
9
4
2
34
10
3
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    print(sol1.answer1())
    assert(sol1.answer1() == 220)
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print(sol.answer1())

    
    if sol1.answer2() == None:
        exit(0)
    print("\n\n\n\t\t PART 2\n\n")
    print(sol1.answer2())
    assert(sol1.answer2() == 19208)
    print("tests 2 Ok !")
    print()
    print(sol.answer2())
