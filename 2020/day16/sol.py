class Field:
    def __init__(self, data):
        self.name, bla = data.split(':')
        int1, _, int2 = bla.split()
        a, b = int1.split('-')
        self.a, self.b = (int(a), int(b))
        a, b = int2.split('-')
        self.aa, self.bb = int(a), int(b)

        self.possib = None

    def contains(self, val):
        return (self.a <= val <= self.b) or (self.aa <= val <= self.bb)

    def __repr__(self):
        return self.name
        

class Solution:
    def __init__(self, data):
        fields, your_ticket, tickets = data[:-1].split("\n\n")

        your_ticket = your_ticket.split('\n')[-1]
        self.your_ticket = tuple(map(int,your_ticket.split(',')))

        self.tickets = set()
        tickets = tickets.split('\n')
        for t in tickets[1:]:
            self.tickets.add(tuple(map(int, t.split(','))))
            
        self.fields = []
        for f in fields.split('\n'):
            self.fields.append(Field(f))

    def clean(self):
        tmp = self.tickets.copy()
        for t in tmp:
            if not all(any(f.contains(v) for f in self.fields) for v in t):
                self.tickets.remove(t)
        
    def answer1(self):
        res = 0
        for t in self.tickets:
            for v in t:
                if not any(f.contains(v) for f in self.fields):
                    res += v
        return res

    def answer2(self, target='departure'):
        self.clean()
        possib = []
        for _ in range(len(self.your_ticket)):
            possib.append([f for f in self.fields])

        for t in self.tickets:
            for v, fields in zip(t, possib):
                to_remove = []
                for f in fields:
                    if not f.contains(v):
                        to_remove.append(f)
                for f in to_remove:
                    fields.remove(f)

        while any(len(p) == 1 for p in possib):
            for i, fields in enumerate(possib):
                if len(fields) == 1:
                    f = fields[0]
                    f.possib = i
                    for p in possib:
                        if f in p:
                            p.remove(f)

        res = 1
        for f in self.fields:
            if f.name[:len(target)] == target:
                res *= self.your_ticket[f.possib]
        return res
        
test_data = """class: 1-3 or 5-7
row: 6-11 or 33-44
seat: 13-40 or 45-50

your ticket:
7,1,14

nearby tickets:
7,3,47
40,4,50
55,2,20
38,6,12
"""

test_data_bis = """class: 0-1 or 4-19
row: 0-5 or 8-19
seat: 0-13 or 16-19

your ticket:
11,12,13

nearby tickets:
3,9,18
15,1,5
5,14,9
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    print(sol1.answer1())
    assert(sol1.answer1() == 71)
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print(sol.answer1())

    
    sol1 = Solution(test_data_bis)
    if sol1.answer2() == None:
        exit(0)
    print("\n\n\n\t\t PART 2\n\n")
    print(sol1.answer2())
    assert(sol1.answer2() == 1)
    print("tests 2 Ok !")
    print()
    print(sol.answer2())
