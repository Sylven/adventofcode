class Solution:
    def __init__(self, data):
        self.data = data[:-1].split("\n\n")

    def answer1(self):
        res = 0
        for answers in self.data:
            S = set(answers.replace('\n',''))
            res += len(S)
        return res
            

    def answer2(self):
        res = 0
        for answers in self.data:
            answers = answers.split('\n')
            S = set(answers[0])
            for row in answers[1:]:
                S.intersection_update(row)
            res += len(S)
        return res
            
test_data = """abc

a
b
c

ab
ac

a
a
a
a

b
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    print(sol1.answer1())
    assert(sol1.answer1() == 11)
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print(sol.answer1())

    
    if sol1.answer2() == None:
        exit(0)
    print("\n\n\n\t\t PART 2\n\n")
    print(sol1.answer2())
    assert(sol1.answer2() == 6)
    print("tests 2 Ok !")
    print()
    print(sol.answer2())
