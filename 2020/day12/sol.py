class Solution:
    action = {
        'N': lambda x,y,val: (x, y+val),
        'E': lambda x,y,val: (x+val, y),
        'S': lambda x,y,val: (x, y-val),
        'W': lambda x,y,val: (x-val, y),
    }

    card = 'NESW'
    
    def __init__(self, data):
        self.data = data.split("\n")[:-1]
        self.pos = (0, 0)
        self.direction = 'E'
        self.waypoint = (10, 1)

    def reset(self):
        self.pos = (0, 0)
        self.direction = 'E'
        self.waypoint = (10, 1)

    def rotate(self, direc, val):
        xx = yy = 1
        x, y = self.waypoint
        if direc == 'R':
            yy = -1
        else:
            xx = -1
        for _ in range(val // 90):
            x, y = xx*y, yy*x
        self.waypoint = x, y
        
    def answer1(self):
        self.reset()
        for row in self.data:
            c, val = row[0], int(row[1:])
            if c == 'F':
                c = self.direction
            if c == 'R':
                self.direction = self.card[(self.card.index(self.direction)+val // 90)%4]
            elif c == 'L':
                self.direction = self.card[(self.card.index(self.direction)-val//90)%4]
            else:
                self.pos = self.action[c](*self.pos, val)
        return abs(self.pos[0]) + abs(self.pos[1])

    def answer2(self):
        self.reset()
        for row in self.data:
            x, y = self.pos
            xx, yy = self.waypoint
            c, val = row[0], int(row[1:])
            if c in 'RL':
                self.rotate(c, val)
                continue
            if c == 'F':
                self.pos = x + val*xx, y + val*yy
            else:
                self.waypoint = self.action[c](*self.waypoint, val)
        return abs(self.pos[0]) + abs(self.pos[1])
        
test_data = """F10
N3
F7
R90
F11
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    print(sol1.answer1())
    assert(sol1.answer1() == 25)
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print(sol.answer1())

    
    if sol1.answer2() == None:
        exit(0)
    print("\n\n\n\t\t PART 2\n\n")
    print(sol1.answer2())
    assert(sol1.answer2() == 286)
    print("tests 2 Ok !")
    print()
    print(sol.answer2())
