class Solution:
    def __init__(self, data):
        self.data = list(map(int,data[:-1].split(",")))
        self.spoken = {}
        self.last = None
        for i, val in enumerate(self.data[:-1]):
            self.spoken[val] = i+1
        self.last = self.data[-1]

    def answer1(self, nb=2020):
        i = max(self.spoken.values())
        while i < nb-1:
            i += 1
            new = self.spoken.get(self.last, 0)
            self.spoken[self.last] = i
            if new == 0:
                self.last = new
            else:
                self.last = i - new
        return self.last

    def answer2(self):
        return self.answer1(30000000)
        
test_data = """0,3,6
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    print(sol1.answer1())
    assert(sol1.answer1() == 436)
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print(sol.answer1())

    
    if sol1.answer2() == None:
        exit(0)
    print("\n\n\n\t\t PART 2\n\n")
    print(sol1.answer2())
    assert(sol1.answer2() == 175594)
    print("tests 2 Ok !")
    print()
    print(sol.answer2())
