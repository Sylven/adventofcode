class Passport:
    def __init__(self, data):
        fields = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid', 'cid']
        self.fields = {f: None for f in fields}

        for info in data.split():
            f, value = info.split(':')
            self.fields[f] = value

    def is_valid(self):
        return not None in [self.fields[f] for f in self.fields if f != 'cid']

    def is_really_valid(self):
        # print()
        # print(self.byr_valid())
        # print(self.iyr_valid())
        # print(self.eyr_valid())
        # print(self.hgt_valid())
        # print(self.hcl_valid())
        # print(self.ecl_valid())
        # print(self.pid_valid())
        return self.byr_valid() and self.iyr_valid() and self.eyr_valid() and self.hgt_valid() and self.hcl_valid() and self.ecl_valid() and self.pid_valid()

    def byr_valid(self):
        byr = self.fields['byr']
        return byr != None and 1920 <= int(byr) <= 2002

    def iyr_valid(self):
        iyr = self.fields['iyr']
        return iyr != None and 2010 <= int(iyr) <= 2020

    def eyr_valid(self):
        eyr = self.fields['eyr']
        return eyr != None and 2020 <= int(eyr) <= 2030

    def hgt_valid(self):
        hgt = self.fields['hgt']
        if hgt == None:
            return False
        value, unit = hgt[:-2], hgt[-2:]
        if unit == 'cm':
            return 150 <= int(value) <= 193
        if unit == 'in':
            return 59 <= int(value) <= 76
        return False

    def hcl_valid(self):
        hcl = self.fields['hcl']
        if hcl == None:
            return False
        if hcl[0] != '#': return False
        value = hcl[1:]
        if len(value) != 6:return False
        for c in value:
            if (not ('0' <= c <= '9')) and (not ('a' <= c <= 'f')):
                return False
        return True

    def ecl_valid(self):
        ecl = self.fields['ecl']
        if ecl == None:
            return False
        return ecl in {'amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'}

    def pid_valid(self):
        pid = self.fields['pid']
        if pid == None:
            return False
        if len(pid) != 9:return False
        return pid.isdigit()
        
class Solution:
    def __init__(self, data):
        data = data[:-1].split("\n\n")
        self.passports = [Passport(p) for p in data]
        
    def answer1(self):
        return sum(p.is_valid() for p in self.passports)

    def answer2(self):
        return sum(p.is_really_valid() for p in self.passports)
        
test_data = """pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
hcl:#623a2f

eyr:2029 ecl:blu cid:129 byr:1989
iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

hcl:#888785
hgt:164cm byr:2001 iyr:2015 cid:88
pid:545766238 ecl:hzl
eyr:2022

iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719

eyr:1972 cid:100
hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

iyr:2019
hcl:#602927 eyr:1967 hgt:170cm
ecl:grn pid:012533040 byr:1946

hcl:dab227 iyr:2012
ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

hgt:59cm ecl:zzz
eyr:2038 hcl:74454a iyr:2023
pid:3556412378 byr:2007
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    print(sol1.answer1())
    #assert(sol1.answer1() == 2)
    #assert(len(sol1.passports) == 4)
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print(sol.answer1())

    
    if sol1.answer2() == None:
        exit(0)
    print("\n\n\n\t\t PART 2\n\n")
    print(sol1.answer2())
    assert(sol1.answer2() == 4)
    print("tests 2 Ok !")
    print()
    print(sol.answer2())
