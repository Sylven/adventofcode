class Seat_sys:
    def __init__(self, data):
        self.data = {(i,j) :c  for (i, row) in enumerate(data) for (j, c) in enumerate(row)}

    def next_step(self):
        new_data = self.data.copy()
        for (i, j), c in new_data.items():
            if c == '.':continue
            neighbors = self.neighbors((i,j))
            if c == 'L':
                if neighbors.count('#') == 0:
                    new_data[(i,j)] = '#'
            elif c == '#':
                if neighbors.count('#') > 3:
                    new_data[(i,j)] = 'L'
        self.data = new_data

    def next_step_v2(self):
        new_data = self.data.copy()
        for (i, j), c in new_data.items():
            if c == '.':continue
            neighbors = self.neighbors_v2((i,j))
            if c == 'L':
                if neighbors.count('#') == 0:
                    new_data[(i,j)] = '#'
            elif c == '#':
                if neighbors.count('#') > 4:
                    new_data[(i,j)] = 'L'
        self.data = new_data

    def neighbors(self, pos):
        i, j = pos
        res = []
        for ii in range(i-1, i+2):
            for jj in range(j-1, j+2):
                if (ii, jj) != pos:
                    res.append(self.data.get((ii, jj), '.'))
        return res

    def neighbors_v2(self, pos):
        i, j = pos
        res = []
        for ii in range(-1, 2):
            for jj in range(-1, 2):
                if (ii, jj) == (0, 0):
                    continue
                k = 1
                while self.data.get((i+k*ii, j+k*jj), 'N') == '.':
                    k += 1
                res.append(self.data.get((i+k*ii, j+k*jj), 'N'))
        return res

    def reset(self):
        for (i, j), c in self.data.items():
            if c == '#':
                self.data[(i,j)] = 'L'

    def to_print(self):
        max_i, max_j = max(self.data)
        for i in range(max_i + 1):
            row = ''
            for j in range(max_j + 1):
                row += self.data[(i, j)]
            print(row)

class Solution:
    def __init__(self, data):
        data = data.split("\n")[:-1]
        self.seats = Seat_sys(data)

    def answer1(self):
        self.seats.reset()
        prev = None
        while prev != self.seats.data:
            prev = self.seats.data
            self.seats.next_step()
        return list(self.seats.data.values()).count('#')

    def answer2(self):
        self.seats.reset()
        prev = None
        while prev != self.seats.data:
            prev = self.seats.data
            # print()
            # self.seats.to_print()
            self.seats.next_step_v2()
        return list(self.seats.data.values()).count('#')
        
test_data = """L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    print(sol1.answer1())
    assert(sol1.answer1() == 37)
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print(sol.answer1())

    
    if sol1.answer2() == None:
        exit(0)
    print("\n\n\n\t\t PART 2\n\n")
    print(sol1.answer2())
    assert(sol1.answer2() == 26)
    print("tests 2 Ok !")
    print()
    print(sol.answer2())
