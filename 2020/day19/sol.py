class Solution:
    def __init__(self, data):
        rules, self.words_list = (para.split('\n') for para in data[:-1].split("\n\n"))
        to_treat = set()
        self.words = {}
        for row in rules:
            nb, r = row.split(': ')
            if r[0] == '"':
                self.words[nb] = r[1]
                continue
            to_treat.add(nb)
            self.words[nb] = set(tuple(w.split()) for w in r.split('|'))
        while to_treat:
            self.initialize(to_treat)

    def initialize(self, to_treat, nb=None):
        if not nb:
            if not to_treat:
                return
            nb = to_treat.pop()
        S = self.words[nb]
        new_S = set()
        for rec_w in S:
            temp_w = set([''])
            for n in rec_w:
                if n in to_treat:
                    to_treat.remove(n)
                    self.initialize(to_treat, n)
                temp = set()
                for w_pref in temp_w:
                    for w_suff in self.words[n]:
                        temp.add(w_pref + w_suff)
                temp_w = temp
            new_S.update(temp_w)
        self.words[nb] = new_S
        return
    
            
    def answer1(self):
        nb = 0
        for w in self.words_list:
            if w in self.words['0']:
                nb += 1
        return nb

    def answer2(self):
        ...
        
test_data = """0: 4 1 5
1: 2 3 | 3 2
2: 4 4 | 5 5
3: 4 5 | 5 4
4: "a"
5: "b"

ababbb
bababa
abbbab
aaabbb
aaaabbb
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    print(sol1.answer1())
    assert(sol1.answer1() == 2)
    print("tests Ok !")
    print()
    
    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print(sol.answer1())

    
    if sol1.answer2() == None:
        exit(0)
    print("\n\n\n\t\t PART 2\n\n")
    print(sol1.answer2())
    assert(sol1.answer2() == None)
    print("tests 2 Ok !")
    print()
    print(sol.answer2())
