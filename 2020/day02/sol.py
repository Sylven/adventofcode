class Solution:
    def __init__(self, data):
        self.data = data.split("\n")[:-1]

    def answer1(self):
        nb = 0
        for row in self.data:
            min_max, l, code = row.split()
            mini, maxi = map(int,min_max.split('-'))
            l = l[:-1]
            c = code.count(l)
            if c <= maxi and c >= mini:
                nb += 1
        return nb

    def answer2(self):
        nb = 0
        for row in self.data:
            min_max, l, code = row.split()
            mini, maxi = map(int,min_max.split('-'))
            mini -= 1
            maxi -= 1
            l = l[:-1]
            if (code[mini] == l or code[maxi] == l) and code[mini] != code[maxi]:
                nb += 1
        return nb
        
test_data = """1-3 a: abcde
1-3 b: cdefg
2-9 c: ccccccccc
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    print(sol1.answer1())
    assert(sol1.answer1() == 2)
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print(sol.answer1())

    
    if sol1.answer2() == None:
        exit(0)
    print("\n\n\n\t\t PART 2\n\n")
    print(sol1.answer2())
    assert(sol1.answer2() == 1)
    print("tests 2 Ok !")
    print()
    print(sol.answer2())
