class Solution:
    def __init__(self, data):
        data = data.split("\n")[:-1]
        self.init = data
        self.grid = {}
        for x, row in enumerate(data):
            for y, c in enumerate(row):
                self.grid[(x,y,0,0)] = c
                

    def reset(self):
        self.grid = {}
        for x, row in enumerate(self.init):
            for y, c in enumerate(row):
                self.grid[(x,y,0,0)] = c
        
    def get_neighbors(self, pos):
        if len(pos) == 3:
            pos = pos + (0,)
        x, y, z, w = pos
        neigh = []
        for xx in range(-1,2):
            for yy in range(-1, 2):
                for zz in range(-1, 2):
                    for ww in range(-1, 2):
                        if (xx, yy, zz, ww) == (0,0,0,0):
                            continue
                        neigh.append(self.grid.get((x+xx,y+yy,z+zz,w+ww), '.'))
        return neigh

    def increase_size(self):
        min_x, min_y, min_z, min_w = min(self.grid)
        max_x, max_y, max_z, max_w = max(self.grid)
        for w in range(min_w-1, max_w+2):
            for z in range(min_z-1, max_z+2):
                for y in range(min_y-1, max_y+2):
                    self.grid[(max_x+1, y, z, w)] = '.'
                    self.grid[(min_x-1, y, z, w)] = '.'
        
        for w in range(min_w-1, max_w+2):
            for z in range(min_z-1, max_z+2):
                for x in range(min_x-1, max_x+2):
                    self.grid[(x, max_y+1, z, w)] = '.'
                    self.grid[(x, min_y-1, z, w)] = '.'
        
        for w in range(min_w-1, max_w+2):
            for x in range(min_x-1, max_x+2):
                for y in range(min_y-1, max_y+2):
                    self.grid[(x, y, max_z+1, w)] = '.'
                    self.grid[(x, y, min_z-1, w)] = '.'

        for z in range(min_z-1, max_z+2):
            for x in range(min_x-1, max_x+2):
                for y in range(min_y-1, max_y+2):
                    self.grid[(x, y, z, min_w-1)] = '.'
                    self.grid[(x, y, z, max_w+1)] = '.'

    def next_step(self):
        self.increase_size()
        temp = self.grid.copy()
        for pos in temp:
            neigh = self.get_neighbors(pos)
            nb_active = neigh.count('#')
            if nb_active == 3 or (nb_active == 2 and self.grid[pos] == '#'):
                temp[pos] = '#'
            else:
                temp[pos] = '.'
        self.grid = temp

    def display(self):
        min_x, min_y, min_z = min(self.grid)
        max_x, max_y, max_z = max(self.grid)
        for z in range(min_z, max_z+1):
            print('\n  z =', z)
            for x in range(min_x, max_x+1):
                print(''.join(self.grid[(x,y,z)] for y in range(min_y, max_y+1)))
            
        
    def answer(self, steps=6):
        self.reset()
        for _ in range(steps):
            self.next_step()
        return tuple(self.grid.values()).count('#')

        
test_data = """.#.
..#
###
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    print(sol1.answer())
    assert(sol1.answer() == 848)
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print(sol.answer())
