class Solution:
    def __init__(self, data):
        self.data = data.split("\n")[:-1]

    def answer1(self, xx = 3, yy = 1):
        x, y = 0,0
        nb = 0
        while y < len(self.data):
            if self.data[y][x] == '#':
                nb += 1
            x = (x + xx) % len(self.data[0])
            y += yy
        return nb

    def answer2(self):
        xxs = (1, 3, 5, 7, 1)
        yys = (1, 1, 1, 1, 2)
        nb = 1
        for xx, yy in zip(xxs, yys):
            nb *= self.answer1(xx, yy)
        return nb
        
test_data = """..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    print(sol1.answer1())
    assert(sol1.answer1() == 7)
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print(sol.answer1())

    
    if sol1.answer2() == None:
        exit(0)
    print("\n\n\n\t\t PART 2\n\n")
    print(sol1.answer2())
    assert(sol1.answer2() == 336)
    print("tests 2 Ok !")
    print()
    print(sol.answer2())
