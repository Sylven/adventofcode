class InfiniteLoop(Exception):
    pass

class Console:
    opperations = {
        'nop': lambda c, val: c._nop(val),
        'acc': lambda c, val: c._acc(val),
        'jmp': lambda c, val: c._jmp(val),
    }
    
    def __init__(self, prog):
        self.accumulator = 0
        self.instruction = 0
        self.programme = []
        for row in prog:
            opp, val = row.split()
            val = int(val)
            self.programme.append((opp, val))

    def _nop(self, val):
        self.instruction += 1

    def _acc(self, val):
        self.accumulator += val
        self.instruction += 1

    def _jmp (self, val):
        self.instruction += val
        
    def next_inst(self):
        if self.instruction >= len(self.programme):
            raise StopIteration()
        opp, val = self.programme[self.instruction]
        self.opperations[opp](self, val)
        return self.accumulator

    def run(self):
        self.reset()
        viewed = set()
        while True:
            if self.instruction in viewed:
                raise InfiniteLoop()
            viewed.add(self.instruction)
            try:
                self.next_inst()
            except StopIteration:
                return self.accumulator

    def reset(self):
        self.accumulator = 0
        self.instruction = 0
                
        

class Solution:
    def __init__(self, data):
        data = data.split("\n")[:-1]
        self.console = Console(data)

    def answer1(self):
        try:
            self.console.run()
        except InfiniteLoop:
            return self.console.accumulator
        
    def answer2(self):
        for i, (opp, val) in enumerate(self.console.programme):
            if opp == 'acc':
                continue
            self.console.programme[i] = ({'nop':'jmp', 'jmp':'nop'}[opp], val)
            try:
                res = self.console.run()
            except InfiniteLoop:
                continue
            else:
                return res
            finally:
                self.console.programme[i] = (opp, val)
        return -1
                
test_data = """nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    print(sol1.answer1())
    assert(sol1.answer1() == 5)
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print(sol.answer1())

    if sol1.answer2() == None:
        exit(0)
    print("\n\n\n\t\t PART 2\n\n")
    print(sol1.answer2())
    assert(sol1.answer2() == 8)
    print("tests 2 Ok !")
    print()
    print(sol.answer2())
