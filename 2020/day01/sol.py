class Solution:
    def __init__(self, data):
        self.data = set(map(int, data.split("\n")[:-1]))

    def answer(self):
        temp = self.data.copy()
        while temp:
            nb = temp.pop()
            for nb2 in temp:
                if 2020-nb-nb2 in self.data:
                    return (2020-nb-nb2) * nb * nb2

        
test_data = """1721
979
366
299
675
1456
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    print(sol1.answer())
    assert(sol1.answer() == 241861950)
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print(sol.answer())
