from collections import defaultdict
class Solution:
    def __init__(self, data):
        data = data.split("\n")[:-1]
        self.graph_contain = defaultdict(set)
        self.graph_contained = defaultdict(set)
        for info in data:
            info = info.split()
            if info[4] == 'no':
                continue
            pos = 7
            color = ' '.join(info[:2])
            while pos < len(info):
                color_contained = ' '.join(info[pos-2:pos])
                self.graph_contain[color].add((color_contained, int(info[pos-3])))
                self.graph_contained[color_contained].add(color)
                pos += 4

    def answer1(self):
        aim = 'shiny gold'
        S = {aim}
        done = set()
        res = -1
        while S:
            color = S.pop()
            done.add(color)
            S.update(self.graph_contained[color].difference(done))
            res += 1
        return res

    def answer2(self):
        start = 'shiny gold'
        S = [(start, 1)]
        res = -1
        while S:
            color, nb = S.pop(-1)
            for color_bis, nb_bis in self.graph_contain[color]:
                S.append((color_bis, nb_bis * nb))
            res += nb
        return res
        

def compute_nb_bags():
    ...
        
test_data = """light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    print(sol1.answer1())
    assert(sol1.answer1() == 4)
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print(sol.answer1())

    
    if sol1.answer2() == None:
        exit(0)
    print("\n\n\n\t\t PART 2\n\n")
    print(sol1.answer2())
    assert(sol1.answer2() == 32)
    print("tests 2 Ok !")
    print()
    print(sol.answer2())
