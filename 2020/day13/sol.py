import math

def lcm(a, b):
    return a * b // math.gcd(a, b)

class Solution:
    def __init__(self, data):
        data = data.split("\n")[:-1]
        self.time = int(data[0])
        self.lines = data[1].split(',')

    def answer1(self):
        res = None
        bus = None
        for Id in self.lines:
            if Id == 'x':
                continue
            Id = int(Id)
            waiting = Id - (self.time % Id)
            if res == None:
                res = waiting
                bus = Id
            else:
                if waiting < res:
                    res = waiting
                    bus = Id
        return res * bus

    def answer2(self):
        steps = 0
        nb = 1
        for i, Id in enumerate(self.lines):
            if Id == 'x':
                continue
            Id = int(Id)
            while steps % Id != (-i) % Id:
                steps += nb
            nb = lcm(nb, Id)
            print(steps)
        return steps
        
test_data = """939
7,13,x,x,59,x,31,19
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    print(sol1.answer1())
    assert(sol1.answer1() == 295)
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print(sol.answer1())

    
    if sol1.answer2() == None:
        exit(0)
    print("\n\n\n\t\t PART 2\n\n")
    print(sol1.answer2())
    assert(sol1.answer2() == 1068781)
    print("tests 2 Ok !")
    print()
    print(sol.answer2())
