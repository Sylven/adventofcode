class Solution:
    def __init__(self, data):
        self.data = data.split("\n")[:-1]

    def answer1(self):
        ...

    def answer2(self):
        ...
        
test_data = """
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    print(sol1.answer1())
    assert(sol1.answer1() == None)
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print(sol.answer1())

    
    if sol1.answer2() == None:
        exit(0)
    print("\n\n\n\t\t PART 2\n\n")
    print(sol1.answer2())
    assert(sol1.answer2() == None)
    print("tests 2 Ok !")
    print()
    print(sol.answer2())
