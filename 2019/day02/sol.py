class Solution:
    def __init__(self, data):
        self.data = list(map(int,data.split(",")))

    def answer1(self, noun=12, verb=2):
        pos = 0
        temp = self.data.copy()
        temp[1] = noun
        temp[2] = verb
        while temp[pos] != 99:
            if temp[pos] == 1:
                temp[temp[pos+3]] = temp[temp[pos+1]] + temp[temp[pos+2]]
            elif temp[pos] == 2:
                temp[temp[pos+3]] = temp[temp[pos+1]] * temp[temp[pos+2]]
            else: raise ValueError("opcode should be 1, 2 or 99. Got %d. pos = %d"%(temp[pos], pos))
            pos += 4
        return temp[0]
    
    def answer2(self):
        for noun in range(100):
            for verb in range(100):
                if self.answer1(noun, verb) == 19690720:
                    return noun * 100 + verb
        
test_data = """1,1,1,4,99,5,6,0,99
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    #print(sol1.answer1())
    #assert(input() == 'y')
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print(sol.answer1())

    
    #if sol1.answer2() == None:
    #    exit(0)
    print("\n\n\n\t\t PART 2\n\n")
    #print(sol1.answer2())
    #assert(sol1.answer2() == None)
    print("tests 2 Ok !")
    print()
    print(sol.answer2())
