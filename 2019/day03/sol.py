class Wire:
    def __init__(self, moves):
        self.pos = set()
        self.steps = {}
        x, y, s = 0, 0, 0
        D = {'U':(0, 1), 'R':(1, 0), 'D': (0, -1), 'L': (-1, 0)}
        for ins in moves.split(','):
            m, i = ins[0], int(ins[1:])
            xx, yy = D[m]
            for _ in range(i):
                s += 1
                x += xx
                y += yy
                self.pos.add((x, y))
                self.steps[(x,y)] = s

class Solution:
    def __init__(self, data):
        data = data.split("\n")[:-1]
        self.w1 = Wire(data[0])
        self.w2 = Wire(data[1])

        self.intersect = self.w1.pos.intersection(self.w2.pos)

    def answer1(self):
        return min(x + y for x, y in self.intersect if x + y != 0)

    def answer2(self):
        return min(self.w1.steps[(x,y)] + self.w2.steps[(x, y)] for x, y in self.intersect)
        
test_data = """R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51
U98,R91,D20,R16,D67,R40,U7,R15,U6,R7
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    print(sol1.answer1())
    assert(sol1.answer1() == 135)
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print(sol.answer1())

    
    if sol1.answer2() == None:
        exit(0)
    print("\n\n\n\t\t PART 2\n\n")
    print(sol1.answer2())
    assert(sol1.answer2() == 410)
    print("tests 2 Ok !")
    print()
    print(sol.answer2())
