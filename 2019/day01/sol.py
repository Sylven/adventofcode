class Solution:
    def __init__(self, data):
        self.data = list(map(int,data.split("\n")[:-1]))

    def answer1(self):
        return sum(d // 3 - 2 for d in self.data)

    def answer2(self):
        return sum(map(fuel, self.data))


def fuel(w):
    res = 0
    while w > 0:
        w = w // 3 - 2
        res += w
    return res - w
    
test_data = """14
1969
100756
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    print(sol1.answer1())
    assert(sol1.answer1() == 33583+2+654)
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print(sol.answer1())

    
    if sol1.answer2() == None:
        exit(0)
    print("\n\n\n\t\t PART 2\n\n")
    print(sol1.answer2())
    assert(sol1.answer2() == 966+2+50346)
    print("tests 2 Ok !")
    print()
    print(sol.answer2())
