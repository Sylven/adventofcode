class Solution:
    def __init__(self, data):
        data = data.split("\n")[0].split('-')
        self.mini, self.maxi = map(int, data)
        
    def answer1(self):
        res = 0
        for nb in range(self.mini, self.maxi):
            res += is_okay(nb)
        return res

    def answer2(self):
        res = 0
        for nb in range(self.mini, self.maxi):
            res += is_okay_2(nb)
        return res


def is_okay_2(nb):
    w = str(nb)
    double = any(w.count(c) == 2 for c in w)
    for i, c in enumerate(w[:-1]):
        if c > w[i+1]:
            return False
    return double
        
def is_okay(nb):
    w = str(nb)
    double = False
    for i, c in enumerate(w[:-1]):
        if c > w[i+1]:
            return False
        if c == w[i+1]:
            double = True
    return double
        
test_data = """
"""

if __name__ == "__main__":
    #sol1 = Solution(test_data)
    #print(sol1.answer1())
    #assert(sol1.answer1() == None)
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print(sol.answer1())

    
    #if sol1.answer2() == None:
    #    exit(0)
    print("\n\n\n\t\t PART 2\n\n")
    #print(sol1.answer2())
    #assert(sol1.answer2() == None)
    #print("tests 2 Ok !")
    print()
    print(sol.answer2())
