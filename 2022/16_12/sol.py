import pdb
import os
import math
import re
import pdb

class Valve:
    def __init__(self, name, rate, others):
        self.name = name
        self.rate = int(rate)
        self.others = set()
        for valve in others:
            self.others.add(valve)
            valve.add_valve(self)
        self.distances = {}

    def __hash__(self):
        return hash(self.name)

    def __eq__(self, other):
        if isinstance(other, Valve):
            return self.name == other.name
        ## checks if the name equals the string
        ## allows `"AA" in self.others` to check if a valve
        ## named "AA" is in the set `self.others`
        return self.name == other

    def __repr__(self):
        return f"'{self.name}'"

    def add_valve(self, other):
        self.others.add(other)

    def initialize_dists(self, valves, viewed=None):
        for v in valves:
            if v in self.others:
                self.distances[v] = 1
                v.distances[self] = 1
        todo = valves.difference(self.distances)
        if self in todo:
            todo.remove(self)
        # all valves already known
        if not todo:
            return

        if viewed is None:
            viewed = set()
        viewed.add(self)
        for v in self.others:
            if v in viewed:
                continue
            v.initialize_dists(todo, viewed)
        viewed.remove(self)

        for other in todo:
            res = None
            for v in self.others:
                if not other in v.distances:
                    continue
                if res is None:
                    res = v.distances[other]
                res = min(res, v.distances[other])
            if not res is None:
                if other == 'AA' and self == 'DD':
                    1/0
                if self == 'AA' and other == 'DD':
                    1/0
                self.distances[other] = res + 1
                other.distances[self] = res + 1

    def best_escape(self, time, visited=None, viewed=None):
        if time < 0:
            return 0, []
        if visited is None:
            visited = set()
        # todo
        if viewed is None:
            viewed = {}

        visited.add(self)
        possib = []
        for valve, dist in self.distances.items():
            if not valve in visited and valve.rate > 0:
                valve_score, path = valve.best_escape(time - dist - 1, visited, viewed)
                possib.append((valve_score, path))
        visited.remove(self)
        if possib:
            res = max(val[0] for val in possib)
            path = None
            for n, p in possib:
                if res == n:
                    path = p
            return res + self.rate*time, [self] + path
        return self.rate*time, []

    def best_escape_2(self, time, max_time, start, visited=None, viewed=None):
        if time < 0:
            elephant = start.best_escape(max_time, visited)[0]
            visited.add(start)
            return elephant
        if visited is None:
            visited = set()
        # todo
        if viewed is None:
            viewed = {}

        visited.add(self)
        possib = []
        for valve, dist in self.distances.items():
            if not valve in visited and valve.rate > 0:
                valve_score = valve.best_escape_2(time - dist - 1, max_time, start, visited, viewed)
                possib.append(valve_score)
        visited.remove(self)
        if possib:
            res = max(possib)
            return res + self.rate*time
        elephant = start.best_escape(max_time, visited)[0]
        visited.add(start)
        return self.rate*time + elephant


class Tunnels:
    def __init__(self, connections):
        self.max_time = 30 ## time to escape
        self.valves = {}
        for name, rate, others in connections:
            viewed_others = [self.valves[v] for v in others.split(', ') if v in self.valves]
            self.valves[name] = Valve(name, rate, viewed_others)

        valves_set = set(self.valves.values())
        for valve in valves_set:
            valve.initialize_dists(valves_set)

    def best_escape(self):
        score, path = self.valves['AA'].best_escape(30)
        return score

    def best_escape_2(self):
        a = self.valves['AA']
        return a.best_escape_2(26, 26, a)



## works but too slow..
## better is to compute the distance between each valves
## and then compute best path between openings
def best_escape_rec(valve, time, opened, viewed):
    if (valve, time, opened) in viewed:
        return viewed[(valve, time, opened)]
    if time == 0:
        return 0
    possib = []
    if not valve in opened and valve.rate > 0:
        possib.append( (time - 1)*valve.rate + best_escapet_rec(valve, time-1, opened+(valve,), viewed))
    for v in valve.others:
        possib.append(best_escapet_rec(v, time-1, opened, viewed))
    res = max(possib)
    viewed[(valve, time, opened)] = res
    return res

    

def solve1(p_input):
    pattern = re.compile(r"^Valve (\w\w) has flow rate=(\d+); \D+ valve[s]? ([\w, ]+)$", re.MULTILINE)
    rows = re.findall(pattern, p_input)
    tunnels = Tunnels(rows)

    return tunnels.best_escape()



def solve2(p_input):
    pattern = re.compile(r"^Valve (\w\w) has flow rate=(\d+); \D+ valve[s]? ([\w, ]+)$", re.MULTILINE)
    rows = re.findall(pattern, p_input)
    tunnels = Tunnels(rows)
    import time
    t = time.time()
    res = tunnels.best_escape_2()
    print(time.time() - t)
    return res

def load_file(path):
    with open(path, 'r') as file:
        result = file.read()
    if result[-2] == '\n\n':
        result = result[:-1]
    return result

def run_tests():
    file = "example_input.txt"
    result_file = "example_results.txt"
    if not os.path.isfile(file)or not os.path.isfile(result_file):
        print("Provide example input and results files")
        return False
    results = load_file(result_file)
    for i, res in enumerate(results.split()):
        answ = eval(f"solve{i+1}(load_file(file))")
        if not answ is None:
            if str(answ) == res:
                print(f"Result of test for question {i+1}: SUCCESS")
            else:
                print(f"Result of test for question {i+1}: FAIL")
                print(f"expeted {res} and got {answ}")
                return False
        else:
            print(f"Question {i+1} not written!")
            return False
    return True

def run_solutions(question=None, both=False):
    file = "input.txt"
    if not os.path.isfile(file):
        print(f"No input file..")
        return
    if question is None and not both:
        if not "solve2" in globals():
            question = 1
        else:
            print("question 1 is skiped")
            question = 2
    if both or question == 1:
        print(f"Result for question 1:", eval(f"solve1(load_file(file))"))
    if both or question == 2:
        if not "solve2" in globals():
            print(f"No function for question 2")
            return
        print(f"Result for question 2:", eval(f"solve2(load_file(file))"))


if __name__ == '__main__':
    if run_tests():
        run_solutions()
