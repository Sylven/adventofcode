import os
import math

class Tree:
    def __init__(self, height, pos=None):
        self.height = height
        self.highest = [None] * 4
        ## first is index second is coordinate that changes
        self.dirs = {
            "N": (0, 0),
            "S": (1, 0),
            "E": (2, 1),
            "W": (3, 1),
        }
        self._is_visible = None
        self.pos = pos

    def is_visible(self):
        if not self._is_visible is None:
            return self._is_visible
        for t in self.highest:
            if t == self:
                self._is_visible = True
                return True
        self._is_visible = False
        return False

    def set_high(self, prev, direction):
        index, _ = self.dirs[direction]
        if prev is None:
            self.highest[index] = self
            return
        if prev.height >= self.height:
            self.highest[index] = prev
            return
        while prev.highest[index] != prev:
            prev = prev.highest[index]
            if prev.height >= self.height:
                self.highest[index] = prev
                return
        self.highest[index] = self

    def __repr__(self):
        p = str(self.pos).replace(', ', '-')
        return f"T_{p}_{self.height}"

    def score(self, max_S, max_E):
        score = 1
        for direction, (index, coordinate) in self.dirs.items():
            t = self.highest[index]
            max_dist = [0, max_S, max_E, 0][index]
            if t is self:
                score *= abs(self.pos[coordinate] - max_dist)
            else:
                score *= abs(self.pos[coordinate] - t.pos[coordinate])
        return score

class Forest:
    def __init__(self, trees):
        self.trees = []
        for i, row in enumerate(trees):
            self.trees.append([])
            for j, t in enumerate(row):
                self.trees[-1].append(Tree(t, (i, j)))
        self.init_higest()

    def init_higest(self):
        for row, prev_row in zip(self.trees, [[None]*len(self.trees[0])] + self.trees):
            for tree, prev_N, prev_W in zip(row, prev_row, [None] + row):
                tree.set_high(prev_N, "N")
                tree.set_high(prev_W, "W")

        trees = self.trees[::-1]
        for row, prev_row in zip(trees, [[None]*len(trees[0])] + trees):
            prev_row, row = prev_row[::-1], row[::-1]
            for tree, prev_S, prev_E in zip(row, prev_row, [None] + row):
                tree.set_high(prev_S, "S")
                tree.set_high(prev_E, "E")
                
    def get_n_visible(self):
        nb = 0
        for i, row in enumerate(self.trees):
            for j, t in enumerate(row):
                if t.is_visible():
                    nb += 1
        return nb

    def get_best_score(self):
        score = 0
        max_S = len(self.trees) - 1
        max_E = len(self.trees[0]) - 1
        for i, row in enumerate(self.trees):
            for j, t in enumerate(row):
                score = max(score, t.score(max_S, max_E))

        return score

def solve1(p_input):
    trees = p_input.split()
    forest = Forest(trees)
    return forest.get_n_visible()

def solve2(p_input):
    trees = p_input.split()
    forest = Forest(trees)
    return forest.get_best_score()

def load_file(path):
    with open(path, 'r') as file:
        result = file.read()
    if result[-2] == '\n\n':
        result = result[:-1]
    return result

def run_tests():
    file = "example_input.txt"
    result_file = "example_results.txt"
    if not os.path.isfile(file)or not os.path.isfile(result_file):
        print("Provide example input and results files")
        return False
    results = load_file(result_file)
    for i, res in enumerate(results.split()):
        answ = eval(f"solve{i+1}(load_file(file))")
        if not answ is None:
            if str(answ) == res:
                print(f"Result of test for question {i+1}: SUCCESS")
            else:
                print(f"Result of test for question {i+1}: FAIL")
                print(f"expeted {res} and got {answ}")
                return False
        else:
            print(f"Question {i+1} not written!")
            return False
    return True

def run_solutions(question=None, both=False):
    file = "input.txt"
    if not os.path.isfile(file):
        print(f"No input file..")
        return False
    if question is None and not both:
        if not "solve2" in globals():
            question = 1
        else:
            print("question 1 is skiped")
            question = 2
    if both or question == 1:
        print(f"Result for question 1:", eval(f"solve1(load_file(file))"))
    if both or question == 2:
        if not "solve2" in globals():
            print(f"No function for question 2")
            return False
        print(f"Result for question 2:", eval(f"solve2(load_file(file))"))
    return True

if __name__ == '__main__':
    if not run_tests():
        print("\n\nTest failed!")
        exit(1)
    else:
       run_solutions(both=True)

