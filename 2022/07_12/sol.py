import os
import math

class File:
    def __init__(self, name, size):
        self.name = name
        self.size = size

    def __hash__(self):
        return hash(self.name)

    def get_size(self):
        return self.size

    def __eq__(self, other):
        return self.name == other.name

class Dir:
    def __init__(self, name, parent = None):
        self.name = name
        self.content = set()
        self.size = None
        self.parent = parent

    def update_content(self, content):
        self.content.add(content)

    def get_size(self):
        if self.size is None:
            self.size = 0
            for f in self.content:
                self.size += f.get_size()
        return self.size

    def get_dir(self, name):
        for f in self.content:
            if f.name == name:
                return f

    def __hash__(self):
        return hash(self.name)

    def __eq__(self, other):
        return self.name == other.name

def build_fs(blocks):
    root = Dir('/')
    current = root
    dirs = [root]

    for b in blocks:
        rows = b.split('\n')
        r = rows[0].split()
        if len(r) == 2:
            cmd, arg = r
        else:
            cmd = r[0]
        if cmd == 'cd':
            if arg == '/':
                current = root
            elif arg == '..':
                current = current.parent
            else:
                current = current.get_dir(arg)
        else: # -> cmd = ls
            temp = current
            if len(r) == 2:
                temp = current.get_dir(arg)
            if not temp.content:
                rows.remove('')
                for r in rows[1:]:
                    a, name = r.split()
                    if a == 'dir':
                        f = Dir(name, temp)
                        dirs.append(f)
                    else:
                        f = File(name, int(a))
                    temp.update_content(f)
    return dirs

def solve1(p_input):
    blocks = p_input.split('$')[1:]
    dirs = build_fs(blocks)

    tot_size = 0
    for d in dirs:
        size = d.get_size()
        if size < 100000:
            tot_size += size

    return tot_size


def solve2(p_input):
    blocks = p_input.split('$')[1:]
    dirs = build_fs(blocks)

    L = list(sorted(d.get_size() for d in dirs))
    free = 70000000 - L[-1]
    for s in L:
        if s + free >= 30000000:
            return s


def load_file(path):
    with open(path, 'r') as file:
        result = file.read()
    if result[-2] == '\n\n':
        result = result[:-1]
    return result

def run_tests():
    file = "example_input.txt"
    result_file = "example_results.txt"
    if not os.path.isfile(file)or not os.path.isfile(result_file):
        print("Provide example input and results files")
        return False
    results = load_file(result_file)
    for i, res in enumerate(results.split()):
        answ = eval(f"solve{i+1}(load_file(file))")
        if not answ is None:
            if str(answ) == res:
                print(f"Result of test for question {i+1}: SUCCESS")
            else:
                print(f"Result of test for question {i+1}: FAIL")
                print(f"expeted {res} and got {answ}")
                return False
        else:
            print(f"Question {i+1} not written!")
            return False
    return True

def run_solutions(question=None, both=False):
    file = "input.txt"
    if not os.path.isfile(file):
        print(f"No input file..")
        return
    if question is None and not both:
        if not "solve2" in globals():
            question = 1
        else:
            print("question 1 is skiped")
            question = 2
    if both or question == 1:
        print(f"Result for question 1:", eval(f"solve1(load_file(file))"))
    if both or question == 2:
        if not "solve2" in globals():
            print(f"No function for question 2")
            return
        print(f"Result for question 2:", eval(f"solve2(load_file(file))"))


if __name__ == '__main__':
    assert run_tests()
    run_solutions()
