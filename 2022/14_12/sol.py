import os
import math


class Engine:
    def __init__(self, p_input):
        self.source = (500, 0)
        self.walls = set()
        for row in p_input.split('\n'):
            if row == '':
                continue
            points = row.split(' -> ')
            #print(get_line(points))
            self.walls.update(get_line(points))
        self.min_index = min(self.walls)[0]
        self.max_index = max(self.walls)[0]
        self.max_ord = max(pos[1] for pos in self.walls)

    def run(self, floor=False):
        i = 0
        while True:
            i += 1
            sand = self.fall(self.source, floor)
            if sand is None:
                return i - 1
            if sand == self.source:
                return i
            self.walls.add(sand)

    def fall(self, pos, floor=False):
        while True:
            x, y = pos
            if not (x, y+1) in self.walls:
                pos = (x, y+1)
            elif not (x-1, y+1) in self.walls:
                pos = (x-1, y+1)
            elif not (x+1, y+1) in self.walls:
                pos = (x+1, y+1)
            else:
                return pos

            if floor:
                if pos[1] >= self.max_ord + 1:
                    return pos
            else:
                if (pos[0] < self.min_index or pos[0] > self.max_index or pos[1] > self.max_ord):
                    return None

def get_line(points):
    L = []
    for el in points:
        a, b = el.split(',')
        L.append((int(a), int(b)))

    if len(L) == 0:
        raise ValueError("points needs to have at least one point")
    if len(L) == 1:
        print("Hmmm.. only one point?")
        return {L[0]}

    ret = set()
    start = L[0]
    for el in L[1:]:
        s_x, e_x = min(start[0], el[0]), max(start[0], el[0])
        s_y, e_y = min(start[1], el[1]), max(start[1], el[1])
        for x in range(s_x, e_x + 1):
            for y in range(s_y, e_y + 1):
                ret.add((x, y))
        start = el
    return ret

def solve1(p_input):
    engine = Engine(p_input)
    return engine.run()

def solve2(p_input):
    engine = Engine(p_input)
    ret =  engine.run(True)
    return ret

def load_file(path):
    with open(path, 'r') as file:
        result = file.read()
    if result[-2] == '\n\n':
        result = result[:-1]
    return result

def run_tests():
    file = "example_input.txt"
    result_file = "example_results.txt"
    if not os.path.isfile(file)or not os.path.isfile(result_file):
        print("Provide example input and results files")
        return False
    results = load_file(result_file)
    for i, res in enumerate(results.split()):
        answ = eval(f"solve{i+1}(load_file(file))")
        if not answ is None:
            if str(answ) == res:
                print(f"Result of test for question {i+1}: SUCCESS")
            else:
                print(f"Result of test for question {i+1}: FAIL")
                print(f"expeted {res} and got {answ}")
                return False
        else:
            print(f"Question {i+1} not written!")
            return False
    return True

def run_solutions(question=None, both=False):
    file = "input.txt"
    if not os.path.isfile(file):
        print(f"No input file..")
        return
    if question is None and not both:
        if not "solve2" in globals():
            question = 1
        else:
            print("question 1 is skiped")
            question = 2
    if both or question == 1:
        print(f"Result for question 1:", eval(f"solve1(load_file(file))"))
    if both or question == 2:
        if not "solve2" in globals():
            print(f"No function for question 2")
            return
        print(f"Result for question 2:", eval(f"solve2(load_file(file))"))


if __name__ == '__main__':
    if run_tests():
        run_solutions()
