import os
import math
import re


class Point(tuple):
    def __init__(self, pos):
        super().__init__()
        if (val := len(self)) != 2:
            raise TypeError(f"tried to initialize a point with dimention different from 2: dim={val}")

    def dist(self, other):
        return abs(self[0] - other[0]) + abs(self[1] - other[1])

class Beacon(Point):
    def __init__(self, pos):
        super().__init__(pos)

class Sensor(Point):
    def __new__(cls, pos, beacon_pos):
        return super().__new__(cls, pos)

    def __init__(self, pos, beacon_pos):
        super().__init__(pos)
        self.beacon = Beacon(beacon_pos)
        self.radius = self.dist(self.beacon)

    def empty_pos(self, row):
        y_diff = abs(self[1] - row)
        start = self[0] - self.radius + y_diff
        end = self[0] + self.radius - y_diff
        res = set((x, row) for x in range(start, end+1))
        if self.beacon in res:
            res.remove(self.beacon)
        return res

    def update_possible_pos(self, possibles):
        min_y = max(0, self[1] - self.radius)
        max_y = min(len(possibles) - 1, self[1] + self.radius)
        for y in range(min_y, max_y+1):
            intervals = possibles[y]
            y_diff = abs(self[1] - y)
            start = self[0] - self.radius + y_diff
            end = self[0] + self.radius - y_diff
            impossible = Interval((start, end))

            new_intervals = []
            for i in intervals:
                new_intervals += i.difference(impossible)
            possibles[y] = new_intervals

class Interval(Point):
    def __init__(self, inter):
        super().__init__(inter)

    def union(self, other):
        """ returns a list of 1 to 2 intervals """
        if self[0] > other[0]:
            return other.union(self)
        if self[1] < other[0]:
            return [self, other]
        if self[1] >= other[1]:
            return [self]
        return [Interval((self[0], other[1]))]

    def difference(self, other):
        # disjoint
        if self[0] > other[1] or self[1] < other[0]:
            return [self]
        # self is included in other
        if self[0] >= other[0] and self[1] <= other[1]:
            return []
        # other strictly included in self
        if self[0] < other[0] and self[1] > other[1]:
            return [Interval((self[0], other[0] - 1)), Interval((other[1] + 1, self[1]))]
        # other around self[0]
        if self[0] >= other[0]:
            return [Interval((other[1] + 1, self[1]))]
        # other around self[1]
        if self[1] <= other[1]:
            return [Interval((self[0], other[0] - 1))]
        print("wtf")

    
class Map:
    def __init__(self, p_input):
        data = p_input.split('\n')
        self.sensors = []
        for row in data:
            ## gets the 4 values in the string
            m = re.match(r"\D+?(-?\d+)"*4, row)
            if m is None:
                continue
            x, y, xx, yy = map(int, m.groups())

            self.sensors.append(Sensor((x, y), (xx, yy)))

    def get_empty(self, row):
        res = set()
        for s in self.sensors:
            res.update(s.empty_pos(row))
        return len(res)

    def get_beacon_freq(self, max_dist):
        possibles = [[Interval((0, max_dist))] for _ in range(max_dist+1)]
        print(len(self.sensors))
        for i, s in enumerate(self.sensors):
            print(f"processing sensor number {i}")
            s.update_possible_pos(possibles)

        for y, interval in enumerate(possibles):
            if len(interval) > 0:
                return interval[0][0] * 4000000 + y

def solve1(p_input, testing=False):
    m = Map(p_input)
    if testing:
        row = 10
    else:
        row = 2000000
    res = m.get_empty(row)
    return res

def solve2(p_input, testing=False):
    m = Map(p_input)
    if testing:
        max_dist = 20
    else:
        max_dist = 4000000
    res = m.get_beacon_freq(max_dist)
    return res


def load_file(path):
    with open(path, 'r') as file:
        result = file.read()
    if result[-2] == '\n\n':
        result = result[:-1]
    return result

def run_tests():
    file = "example_input.txt"
    result_file = "example_results.txt"
    if not os.path.isfile(file)or not os.path.isfile(result_file):
        print("Provide example input and results files")
        return False
    results = load_file(result_file)
    for i, res in enumerate(results.split()):
        answ = eval(f"solve{i+1}(load_file(file), True)")
        if not answ is None:
            if str(answ) == res:
                print(f"Result of test for question {i+1}: SUCCESS")
            else:
                print(f"Result of test for question {i+1}: FAIL")
                print(f"expeted {res} and got {answ}")
                return False
        else:
            print(f"Question {i+1} not written!")
            return False
    return True

def run_solutions(question=None, both=False):
    file = "input.txt"
    if not os.path.isfile(file):
        print(f"No input file..")
        return
    if question is None and not both:
        if not "solve2" in globals():
            question = 1
        else:
            print("question 1 is skiped")
            question = 2
    if both or question == 1:
        print(f"Result for question 1:", eval(f"solve1(load_file(file))"))
    if both or question == 2:
        if not "solve2" in globals():
            print(f"No function for question 2")
            return
        print(f"Result for question 2:", eval(f"solve2(load_file(file))"))


if __name__ == '__main__':
    if run_tests():
        run_solutions()
