import os
import math

from string import ascii_letters

def solve1(p_input):
    score = 0
    for bag in p_input.split():
        first_pkt, second_pkt = bag[:len(bag)//2], bag[len(bag)//2:]
        letter = set(first_pkt).intersection(second_pkt)
        score += ascii_letters.index(letter.pop()) + 1
    return score

def solve2(p_input):
    score = 0
    p_input = iter(p_input.split())
    for el_1, el_2, el_3 in zip(p_input, p_input, p_input):
        letter = set(el_1).intersection(el_2).intersection(el_3)
        score += ascii_letters.index(letter.pop()) + 1
    return score


def load_file(path):
    with open(path, 'r') as file:
        result = file.read()
    if result[-2] == '\n\n':
        restult = result[:-1]
    return result

def run_tests():
    file = "example_input.txt"
    result_file = "example_results.txt"
    if not os.path.isfile(file)or not os.path.isfile(result_file):
        print("Provide example input and results files")
        return
    results = load_file(result_file)
    for i, res in enumerate(results.split()):
        answ = eval(f"solve{i+1}(load_file(file))")
        if not answ is None:
            if str(answ) == res:
                print(f"Result of test for question {i+1}: SUCCESS")
            else:
                print(f"Result of test for question {i+1}: FAIL")
                print(f"expeted {res} and got {answ}")
                return
        else:
            print(f"Question {i+1} not written!")
            return

def run_solutions(question=None, both=False):
    file = "input.txt"
    if not os.path.isfile(file):
        print(f"No input file..")
        return
    if question is None and not both:
        if not "solve2" in globals():
            question = 1
        else:
            print("question 1 is skiped")
            question = 2
    if both or question == 1:
        print(f"Result for question 1:", eval(f"solve1(load_file(file))"))
    if both or question == 2:
        if not "solve2" in globals():
            print(f"No function for question 2")
            return
        print(f"Result for question 2:", eval(f"solve2(load_file(file))"))


if __name__ == '__main__':
    run_tests()
    run_solutions()
