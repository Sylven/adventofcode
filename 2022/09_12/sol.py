import os
import math

class Point(tuple):
    def __init__(self, it):
        super().__init__()
        if len(it) != 2:
            raise TypeError(f"Point expected iterable of length 2, got {len(it)}")

    def __add__(self, other):
        if isinstance(other, tuple):
            if len(other) != 2:
                raise TypeError(f"Point expected iterable of length 2, got {len(it)}")
            return Point((self[0] + other[0], self[1] + other[1]))
        raise TypeError(f"other must by of type Point or tuple, got {type(other)}")

    def __neg__(self):
        return Point((-self[0], -self[1]))

    def __sub__(self, other):
        return self + (-Point(other))

    def __abs__(self):
        return Point((abs(self[0]), abs(self[1])))

    def max(self, other):
        return Point((max(self[0], other[0]), max(self[1], other[1])))

    def min(self, other):
        return Point((min(self[0], other[0]), min(self[1], other[1])))

    def dist(self, other):
        diff = self - other
        return max(abs(diff))

    def move_towards(self, other):
        if self.dist(other) < 2:
            return self
        diff = other - self
        diff = diff.max( (-1, -1))
        diff = diff.min( (1, 1))
        return self + diff
        

class Bridge:
    def __init__(self, length=2):
        self.tails = [Point((0, 0)) for _ in range(length-1)]
        self.head = Point((0, 0))
        self.visited = {self.tails[-1]}
        self.directions = {
            'U': Point((0, 1)),
            'D': Point((0, -1)),
            'R': Point((1, 0)),
            'L': Point((-1, 0))
        }

    def move(self, d):
        vect = self.directions[d]
        self.head += vect
        prev = self.head
        for i, t in enumerate(self.tails):
            self.tails[i] = t.move_towards(prev)
            prev = self.tails[i]
            
        self.visited.add(self.tails[-1])
        
            
    def get_nb_visited(self):
        return len(self.visited)

def solve1(p_input):
    bridge = Bridge()
    moves = iter(p_input.split())

    for d, nb in zip(moves, moves):
        nb = int(nb)
        for _ in range(nb):
            bridge.move(d)
    
    return bridge.get_nb_visited()
    

def solve2(p_input):
    bridge = Bridge(10)
    moves = iter(p_input.split())

    for d, nb in zip(moves, moves):
        nb = int(nb)
        for _ in range(nb):
            bridge.move(d)
    
    return bridge.get_nb_visited()
    


def load_file(path):
    with open(path, 'r') as file:
        result = file.read()
    if result[-2] == '\n\n':
        result = result[:-1]
    return result

def run_tests():
    file = "example_input.txt"
    result_file = "example_results.txt"
    if not os.path.isfile(file)or not os.path.isfile(result_file):
        print("Provide example input and results files")
        return False
    results = load_file(result_file)
    for i, res in enumerate(results.split()):
        answ = eval(f"solve{i+1}(load_file(file))")
        if not answ is None:
            if str(answ) == res:
                print(f"Result of test for question {i+1}: SUCCESS")
            else:
                print(f"Result of test for question {i+1}: FAIL")
                print(f"expeted {res} and got {answ}")
                return False
        else:
            print(f"Question {i+1} not written!")
            return False
    return True

def run_solutions(question=None, both=False):
    file = "input.txt"
    if not os.path.isfile(file):
        print(f"No input file..")
        return
    if question is None and not both:
        if not "solve2" in globals():
            question = 1
        else:
            print("question 1 is skiped")
            question = 2
    if both or question == 1:
        print(f"Result for question 1:", eval(f"solve1(load_file(file))"))
    if both or question == 2:
        if not "solve2" in globals():
            print(f"No function for question 2")
            return
        print(f"Result for question 2:", eval(f"solve2(load_file(file))"))


if __name__ == '__main__':
    if run_tests():
        run_solutions()
