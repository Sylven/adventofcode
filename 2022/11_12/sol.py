import os
import math

class Monkey:
    def __init__(self, _id, items, op, test, res, monkeys):
        self._id = _id
        self.items = items or []
        self.op = op
        self.test = test
        self.res = res
        self.cnt = 0

        self.monkeys = monkeys
        
    def turn(self, divide=True, ppcm=None):
        for old in self.items:
            self.cnt += 1
            worry = eval(self.op)
            if divide:
                worry = math.floor(worry // 3)
            cond = (worry % self.test) == 0
            n_monkey = self.res[cond]
            if ppcm is not None:
                worry %= ppcm
            self.monkeys[n_monkey].items.append(worry)
        self.items = []

class Game:
    def __init__(self, p_input):
        monkeys = []
        for m in p_input.strip().split('\n\n'):
            _id, items, op, test, *res = m.split('\n')
            _id = int(_id.split()[-1][:-1])
            _, items = items.split(':')
            items = list(map(int, items.replace(',', '').split()))
            _, op = op.split('=')
            test = int(test.split()[-1])
            res = [int(row.split()[-1]) for row in res][::-1]
            monkeys.append( Monkey(_id, items, op, test, res, monkeys))
        self.monkeys = monkeys
        self.div_lcm = math.lcm(*[m.test for m in self.monkeys])

    def round(self, divide=True, with_lcm=None):
        lcm = None
        if with_lcm:
            lcm = self.div_lcm
        for m in self.monkeys:
            m.turn(divide, lcm)
        
    def run(self, n_rounds=20, divide=True, with_lcm=False):
        for _ in range(n_rounds):
            self.round(divide, with_lcm)

    def get_monkey_business(self):
        L = sorted(self.monkeys, key=lambda m: m.cnt, reverse=True)
        return L[0].cnt * L[1].cnt

def solve1(p_input):
    game = Game(p_input)
    game.run()
    return game.get_monkey_business()

def solve2(p_input):
    game = Game(p_input)
    game.run(10000, divide=False, with_lcm=True)
    return game.get_monkey_business()


def load_file(path):
    with open(path, 'r') as file:
        result = file.read()
    if result[-2] == '\n\n':
        result = result[:-1]
    return result

def run_tests():
    file = "example_input.txt"
    result_file = "example_results.txt"
    if not os.path.isfile(file)or not os.path.isfile(result_file):
        print("Provide example input and results files")
        return False
    results = load_file(result_file)
    for i, res in enumerate(results.split()):
        answ = eval(f"solve{i+1}(load_file(file))")
        if not answ is None:
            if str(answ) == res:
                print(f"Result of test for question {i+1}: SUCCESS")
            else:
                print(f"Result of test for question {i+1}: FAIL")
                print(f"expeted {res} and got {answ}")
                return False
        else:
            print(f"Question {i+1} not written!")
            return False
    return True

def run_solutions(question=None, both=False):
    file = "input.txt"
    if not os.path.isfile(file):
        print(f"No input file..")
        return
    if question is None and not both:
        if not "solve2" in globals():
            question = 1
        else:
            print("question 1 is skiped")
            question = 2
    if both or question == 1:
        print(f"Result for question 1:", eval(f"solve1(load_file(file))"))
    if both or question == 2:
        if not "solve2" in globals():
            print(f"No function for question 2")
            return
        print(f"Result for question 2:", eval(f"solve2(load_file(file))"))


if __name__ == '__main__':
    if run_tests():
        run_solutions()
