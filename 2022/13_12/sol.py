import os
import math

class SmallerError(Exception):
    pass

class BiggerError(Exception):
    pass

class Packet(list):
    def __init__(self, data):
        super().__init__(data)

    def __lt__(self, other):
        try:
            comparator(self, other)
        except SmallerError:
            return True
        except BiggerError:
            return False

def comparator(L, LL):
    if isinstance(L, int) and isinstance(LL, int):
        if L < LL:
            raise SmallerError()
        if LL < L:
            raise BiggerError()
        return
    if isinstance(L, int):
        return comparator([L], LL)
    if isinstance(LL, int):
        return comparator(L, [LL])
    for u, uu in zip(L, LL):
        comparator(u, uu)
    if len(L) < len(LL):
        raise SmallerError()
    if len(LL) < len(L):
        raise BiggerError()

def solve1(p_input):
    pairs = p_input.split('\n\n')
    res = 0
    if pairs[-1] == []:
        pairs = pairs[:-1]
    for i, pair in enumerate(pairs):
        L, LL = map(eval, pair.split())
        try:
            comparator(L, LL)
        except SmallerError:
            res += i+1
        except BiggerError:
            continue
    return res
        
def solve2(p_input):
    packets = [Packet(eval(row)) for row in p_input.split()]

    a = Packet([[2]])
    b = Packet([[6]])
    packets.append(a)
    packets.append(b)

    packets.sort()
    return (packets.index(a) + 1) * (packets.index(b) + 1)


def load_file(path):
    with open(path, 'r') as file:
        result = file.read()
    if result[-2] == '\n\n':
        result = result[:-1]
    return result

def run_tests():
    file = "example_input.txt"
    result_file = "example_results.txt"
    if not os.path.isfile(file)or not os.path.isfile(result_file):
        print("Provide example input and results files")
        return False
    results = load_file(result_file)
    for i, res in enumerate(results.split()):
        answ = eval(f"solve{i+1}(load_file(file))")
        if not answ is None:
            if str(answ) == res:
                print(f"Result of test for question {i+1}: SUCCESS")
            else:
                print(f"Result of test for question {i+1}: FAIL")
                print(f"expeted {res} and got {answ}")
                return False
        else:
            print(f"Question {i+1} not written!")
            return False
    return True

def run_solutions(question=None, both=False):
    file = "input.txt"
    if not os.path.isfile(file):
        print(f"No input file..")
        return
    if question is None and not both:
        if not "solve2" in globals():
            question = 1
        else:
            print("question 1 is skiped")
            question = 2
    if both or question == 1:
        print(f"Result for question 1:", eval(f"solve1(load_file(file))"))
    if both or question == 2:
        if not "solve2" in globals():
            print(f"No function for question 2")
            return
        print(f"Result for question 2:", eval(f"solve2(load_file(file))"))


if __name__ == '__main__':
    if run_tests():
        run_solutions()
