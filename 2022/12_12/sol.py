import os
import math

from collections import deque
from string import ascii_lowercase

letters = ascii_lowercase

def solve1(p_input):
    Map = [list(row) for row in p_input.split()]
    res = [[-1 for _ in row] for row in Map]
    todo = deque()
    viewed = set()
    start = end = None
    for i, row in enumerate(Map):
        for j, c in enumerate(row):
            if c == 'S':
                start = (i, j, 'a')
                res[i][j] = 0
                viewed.add((i, j))
            elif c == 'E':
                end = (i, j)
    todo.append(start)
    i, j = end
    Map[i][j] = 'z'

    while todo:
        i, j, c = todo.popleft()
        index = letters.index(c)
        dist = res[i][j] + 1
        for ii, jj in ((i-1, j), (i+1, j), (i, j-1), (i, j+1)):
            if ii < 0 or jj < 0 or ii >= len(Map) or jj >= len(Map[0]):
                continue
            if (ii, jj) in viewed:
                continue
            cc = Map[ii][jj]
            new_index = letters.index(cc)
            if new_index <= index + 1:
                if (ii, jj) == end:
                    return dist
                viewed.add((ii, jj))
                res[ii][jj] = dist
                todo.append((ii, jj, cc))

def solve2(p_input):
    Map = [list(row) for row in p_input.split()]
    res = [[-1 for _ in row] for row in Map]
    todo = deque()
    viewed = set()
    start = end = None
    for i, row in enumerate(Map):
        for j, c in enumerate(row):
            if c == 'S':
                start = (i, j, 'a')
            elif c == 'E':
                end = (i, j, 'z')
                res[i][j] = 0
                viewed.add((i, j))
    todo.append(end)
    i, j , c = start
    Map[i][j] = c

    while todo:
        i, j, c = todo.popleft()
        index = letters.index(c)
        dist = res[i][j] + 1
        for ii, jj in ((i-1, j), (i+1, j), (i, j-1), (i, j+1)):
            if ii < 0 or jj < 0 or ii >= len(Map) or jj >= len(Map[0]):
                continue
            if (ii, jj) in viewed:
                continue
            cc = Map[ii][jj]
            new_index = letters.index(cc)
            if new_index >= index - 1:
                if cc == 'a':
                    return dist
                viewed.add((ii, jj))
                res[ii][jj] = dist
                todo.append((ii, jj, cc))


def load_file(path):
    with open(path, 'r') as file:
        result = file.read()
    if result[-2] == '\n\n':
        result = result[:-1]
    return result

def run_tests():
    file = "example_input.txt"
    result_file = "example_results.txt"
    if not os.path.isfile(file)or not os.path.isfile(result_file):
        print("Provide example input and results files")
        return False
    results = load_file(result_file)
    for i, res in enumerate(results.split()):
        answ = eval(f"solve{i+1}(load_file(file))")
        if not answ is None:
            if str(answ) == res:
                print(f"Result of test for question {i+1}: SUCCESS")
            else:
                print(f"Result of test for question {i+1}: FAIL")
                print(f"expeted {res} and got {answ}")
                return False
        else:
            print(f"Question {i+1} not written!")
            return False
    return True

def run_solutions(question=None, both=False):
    file = "input.txt"
    if not os.path.isfile(file):
        print(f"No input file..")
        return
    if question is None and not both:
        if not "solve2" in globals():
            question = 1
        else:
            print("question 1 is skiped")
            question = 2
    if both or question == 1:
        print(f"Result for question 1:", eval(f"solve1(load_file(file))"))
    if both or question == 2:
        if not "solve2" in globals():
            print(f"No function for question 2")
            return
        print(f"Result for question 2:", eval(f"solve2(load_file(file))"))


if __name__ == '__main__':
    if run_tests():
        run_solutions()
