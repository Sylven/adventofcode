import os
import math

class EndOfExecutionError(Exception):
    def __init__(self, msg):
        super().__init__()

class CPU:
    def __init__(self, instructions):
        self.instructions = instructions
        self.X = 1
        self.cycle = 0
        self.addx = None
        self.index = 0
        self.CRT = [['' for _ in range(40)] for _ in range(6)]

    def execute(self):
        self.cycle += 1
        # we are in the second tick of a addx
        if not self.addx is None:
            self.X += self.addx
            self.addx = None
            return True
        if self.index >= len(self.instructions):
            return False
        ins = self.instructions[self.index].split()
        self.index += 1
        if ins[0] == 'addx':
            self.addx = int(ins[1])
        return True

    def get_score(self):
        res = 0
        while self.execute():
            if self.cycle in [19, 59, 99, 139, 179, 219]:
                res += self.X * (self.cycle + 1)
        return res

    def display_CRT(self):
        for i in range(240):
            row, col = divmod(i, 40)
            # if col aligned with X
            if abs(self.X - col) <= 1:
                pixel = '#'
            else:
                pixel = '.'
            self.CRT[row][col] = pixel
            self.execute()
        CRT = ["".join(row) for row in self.CRT]
        print("\n".join(CRT))
        return CRT

def solve1(p_input):
    instructions = p_input.split('\n')
    if '' in instructions:
        instructions.remove('')

    cpu = CPU(instructions)
    return cpu.get_score()

def solve2(p_input):
    instructions = p_input.split('\n')
    if '' in instructions:
        instructions.remove('')

    cpu = CPU(instructions)
    return ''.join(cpu.display_CRT())

def load_file(path):
    with open(path, 'r') as file:
        result = file.read()
    if result[-2] == '\n\n':
        result = result[:-1]
    return result

def run_tests():
    file = "example_input.txt"
    result_file = "example_results.txt"
    if not os.path.isfile(file)or not os.path.isfile(result_file):
        print("Provide example input and results files")
        return False
    results = load_file(result_file)
    for i, res in enumerate(results.split()):
        answ = eval(f"solve{i+1}(load_file(file))")
        if not answ is None:
            if str(answ) == res:
                print(f"Result of test for question {i+1}: SUCCESS")
            else:
                print(f"Result of test for question {i+1}: FAIL")
                print(f"expeted {res} and got {answ}")
                return False
        else:
            print(f"Question {i+1} not written!")
            return False
    return True

def run_solutions(question=None, both=False):
    file = "input.txt"
    if not os.path.isfile(file):
        print(f"No input file..")
        return
    if question is None and not both:
        if not "solve2" in globals():
            question = 1
        else:
            print("question 1 is skiped")
            question = 2
    if both or question == 1:
        print(f"Result for question 1:", eval(f"solve1(load_file(file))"))
    if both or question == 2:
        if not "solve2" in globals():
            print(f"No function for question 2")
            return
        print(f"Result for question 2:", eval(f"solve2(load_file(file))"))


if __name__ == '__main__':
    if run_tests():
        run_solutions()
