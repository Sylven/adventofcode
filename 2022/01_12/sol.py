import os
import math

def solve1(p_input):
    elves = p_input.split('\n\n')

    max_cal = 0
    for elf in elves:
        cal = sum(map(int, elf.split()))
        if cal > max_cal:
            max_cal = cal
    return max_cal

def solve2(p_input):
    elves = [sum(map(int,elf.split())) for elf in p_input.split('\n\n')]
    elves.sort()
    return sum(elves[-3:])


def load_file(path):
    with open(path, 'r') as file:
        result = file.read()
    return result

def run_tests():
    file = "example_input.txt"
    result_file = "example_results.txt"
    if not os.path.isfile(file)or not os.path.isfile(result_file):
        print("Provide example input and results files")
        return
    results = load_file(result_file)
    for i, res in enumerate(results.split()):
        answ = eval(f"solve{i+1}(load_file(file))")
        if not answ is None:
            if str(answ) == res:
                print(f"Result of test for question {i+1}: SUCCESS")
            else:
                print(f"Result of test for question {i+1}: FAIL")
                print(f"expeted {res} and got {answ}")
                return
        else:
            print(f"Question {i+1} not written!")
            return

def run_solutions(question=None, both=False):
    file = "input.txt"
    if not os.path.isfile(file):
        print(f"No input file..")
        return
    if question is None and not both:
        if not "solve2" in globals():
            question = 1
        else:
            print("question 1 is skiped")
            question = 2
    if both or question == 1:
        print(f"Result for question 1:", eval(f"solve1(load_file(file))"))
    if both or question == 2:
        if not "solve2" in globals():
            print(f"No function for question 2")
            return
        print(f"Result for question 2:", eval(f"solve2(load_file(file))"))


if __name__ == '__main__':
    run_tests()
    run_solutions()
