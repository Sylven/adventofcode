import os
import math

def init_config(config):
    config = config.split('\n')[-2::-1]
    size = len(config[0].split())
    lines = [[] for _ in range(size)]
    for row in config:
        crates = [row[4*i + 1] for i in range(size)]
        for c, l in zip(crates, lines):
            if c != ' ':
                l.append(c)
    return lines

def solve1(p_input):
    config, plan = p_input.split('\n\n')
    lines = init_config(config)

    plan = plan.split('\n')
    if not plan[-1]:
        plan = plan[:-1]
    for rule in plan:
        _, nb, _, in_l, _, out_l = rule.split()
        nb, in_l, out_l = int(nb), int(in_l)-1, int(out_l)-1
        for _ in range(nb):
            c = lines[in_l].pop()
            lines[out_l].append(c)

    res = ''
    for l in lines:
        res += l[-1]
    return res

def solve2(p_input):
    config, plan = p_input.split('\n\n')
    lines = init_config(config)

    plan = plan.split('\n')
    if not plan[-1]:
        plan = plan[:-1]
    for rule in plan:
        _, nb, _, in_l, _, out_l = rule.split()
        nb, in_l, out_l = int(nb), int(in_l)-1, int(out_l)-1

        lines[out_l] += lines[in_l][-nb:]
        lines[in_l] = lines[in_l][:-nb]

    res = ''
    for l in lines:
        res += l[-1]
    return res


def load_file(path):
    with open(path, 'r') as file:
        result = file.read()
    if result[-2] == '\n\n':
        result = result[:-1]
    return result

def run_tests():
    file = "example_input.txt"
    result_file = "example_results.txt"
    if not os.path.isfile(file)or not os.path.isfile(result_file):
        print("Provide example input and results files")
        return
    results = load_file(result_file)
    for i, res in enumerate(results.split()):
        answ = eval(f"solve{i+1}(load_file(file))")
        if not answ is None:
            if str(answ) == res:
                print(f"Result of test for question {i+1}: SUCCESS")
            else:
                print(f"Result of test for question {i+1}: FAIL")
                print(f"expeted {res} and got {answ}")
                return
        else:
            print(f"Question {i+1} not written!")
            return

def run_solutions(question=None, both=False):
    file = "input.txt"
    if not os.path.isfile(file):
        print(f"No input file..")
        return
    if question is None and not both:
        if not "solve2" in globals():
            question = 1
        else:
            print("question 1 is skiped")
            question = 2
    if both or question == 1:
        print(f"Result for question 1:", eval(f"solve1(load_file(file))"))
    if both or question == 2:
        if not "solve2" in globals():
            print(f"No function for question 2")
            return
        print(f"Result for question 2:", eval(f"solve2(load_file(file))"))


if __name__ == '__main__':
    run_tests()
    run_solutions()
