class Box:
    def __init__(self, size):
        self.l, self.L, self.h = size

    def surface(self):
        return 2* (self.l*self.L + self.l*self.h + self.L*self.h)

    def paper(self):
        return self.surface() + min([self.l*self.L, self.l*self.h, self.L*self.h])

    def ribbon(self):
        p =  2*min(self.l + self.L, self.l+self.h, self.L + self.h)
        return p + self.l*self.L*self.h

class Solution:
    def __init__(self, data):
        data = data.split("\n")[:-1]
        self.presents = []
        for b in data:
            self.presents.append(Box([int(i) for i in b.split("x")]))


    def answer1(self):
        return sum(b.paper() for b in self.presents)

    def answer2(self):
        return sum(b.ribbon() for b in self.presents)
        
test_data = """2x3x4
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    print(sol1.answer1())
    print(sol1.answer2())
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print(sol.answer1())
    print(sol.answer2())
    
