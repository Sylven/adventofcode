class Solution:
    def __init__(self, data):
        data = data.split("\n")[:-1]
        self.data = data[0]

    def answer1(self):
        return self.data.count("(") - self.data.count(")")

    def answer2(self):
        n = 0
        for i, c in enumerate(self.data):
            n += {"(": 1, ")":-1}[c]
            if n == -1:
                return i+1
        return -1
        
test_data = """(((
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    print(sol1.answer1())
    #assert(sol1.answer2() == ("c", 4))
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print(sol.answer1())
    print(sol.answer2())
    
