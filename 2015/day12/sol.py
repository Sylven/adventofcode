import re
import json
import sys


class Solution:
    def __init__(self, data):
        self.data = data[:-1]
        self.data_json = json.loads(self.data)

    def value(self, item):
        res = 0
        if isinstance(item, dict) and "red" in item.values():
            return 0
        if isinstance(item, dict):
            item = item.values()
        for c in item:
            if isinstance(c, int):
                res += c
            elif isinstance(c, (list, dict)):
                res += self.value(c)
        return res

    def answer1(self):
        return sum(int(i) for i in re.findall(r"-?\d+",self.data) or [0])

    def answer2(self):
        return self.value(self.data_json)
        
test_data = """[1,"red",5]
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    print(sol1.answer1())
    print(sol1.answer2())
    #assert(sol1.answer2() == ("c", 4))
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print(sol.answer1())
    print(sol.answer2())
    
