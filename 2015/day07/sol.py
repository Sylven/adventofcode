from collections import defaultdict
import re

class BitwiseItem:
    def __init__(self):
        self.value = None

    def get_value(self):
        if self.value:
            return self.value
        self.value = self._get_value()
        return self.value

class Wire(BitwiseItem):
    def __init__(self):
        BitwiseItem.__init__(self)
        self.source = None

    def _get_value(self):
        return self.source.get_value()

class Value(BitwiseItem):
    def __init__(self, v):
        BitwiseItem.__init__(self)
        self.value = v

    def get_value(self):
        return self.value

class Not(BitwiseItem):
    def __init__(self, x):
        BitwiseItem.__init__(self)
        self.x = x

    def get_value(self):
        return 65535 - self.x.get_value()
    
class And(BitwiseItem):
    def __init__(self, x, y):
        BitwiseItem.__init__(self)
        self.x = x
        self.y = y

    def get_value(self):
        return self.x.get_value() & self.y.get_value()

class Or(BitwiseItem):
    def __init__(self, x, y):
        BitwiseItem.__init__(self)
        self.x = x
        self.y = y

    def get_value(self):
        return self.x.get_value() | self.y.get_value()
    
class Lshift(BitwiseItem):
    def __init__(self, x, y):
        BitwiseItem.__init__(self)
        self.x = x
        self.y = y

    def get_value(self):
        return self.x.get_value() << self.y.get_value()

class Rshift(BitwiseItem):
    def __init__(self, x, y):
        BitwiseItem.__init__(self)
        self.x = x
        self.y = y

    def get_value(self):
        return self.x.get_value() >> self.y.get_value()

class Solution:
    def __init__(self, data):
        self.data = data.split("\n")[:-1]
        self.wires = defaultdict(Wire)
        for inst in self.data:
            expr, res = inst.split(" -> ")
            args = re.findall(r"[0-9a-z]+", expr)
            for i, a in enumerate(args):
                if a.isdigit():
                    args[i] = Value(int(a))
                else:
                    args[i] = self.wires[a]
            op = re.search(r"[A-Z]+", expr)
            if op == None:
                self.wires[res].source = args[i]
            else:
                op = eval(op.group(0).capitalize() + "(*args)")
                self.wires[res].source = op
            
                

    def answer1(self, v):
        return self.wires[v].get_value()
    
            

    def answer2(self, u, v):
        self.wires[v].value = self.wires[u].value
        for c, w in self.wires.items():
            if c != v:
                w.value = None
        return self.wires[u].get_value()
        
test_data = """123 -> x
456 -> y
x AND y -> d
x OR y -> e
x LSHIFT 2 -> f
y RSHIFT 2 -> g
NOT x -> h
NOT y -> i
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    print(sol1.answer1("i"))
    #assert(sol1.answer2() == ("c", 4))
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print(sol.answer1("a"))
    print(sol.answer2("a", "b"))
    
