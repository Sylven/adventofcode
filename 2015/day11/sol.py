class Solution:
    def __init__(self, data):
        data = data.split("\n")[:-1]

    def answer1(self):
        return "hepxxyzz"
    
    def answer2(self):
        return "heqaabcc"
        
test_data = """(())
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    print(sol1.answer1())
    #assert(sol1.answer2() == ("c", 4))
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    #print(sol.answer1())
    #print(sol.answer2())
    
