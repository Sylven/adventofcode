from itertools import *
from collections import defaultdict
import re

class Solution:
    def __init__(self, data):
        data = data.split("\n")[:-1]
        self.distances = defaultdict(dict)
        self.cities = set()
        for info in data:
            nodes, dist = info.split(" = ")
            dist = int(dist)
            c1, c2 = nodes.split(" to ")
            self.cities.update({c1, c2})
            self.distances[c1][c2] = dist
            self.distances[c2][c1] = dist
        

    def answer1(self):
        res = None
        for c1, c2 in combinations(self.cities, 2):
            for cities in permutations(self.cities.difference({c1,c2})):
                s = self.distances[c1][cities[0]] + self.distances[c2][cities[-1]]
                for i, c in enumerate(cities[:-1]):
                    s += self.distances[c][cities[i+1]]
                if res == None:
                    res = s
                res = min(res, s)
        return res
                

    def answer2(self):
        res = None
        for c1, c2 in combinations(self.cities, 2):
            for cities in permutations(self.cities.difference({c1,c2})):
                s = self.distances[c1][cities[0]] + self.distances[c2][cities[-1]]
                for i, c in enumerate(cities[:-1]):
                    s += self.distances[c][cities[i+1]]
                if res == None:
                    res = s
                res = max(res, s)
        return res
        
test_data = """London to Dublin = 464
London to Belfast = 518
Dublin to Belfast = 141
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    print(sol1.answer1())
    print(sol1.answer2())
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print(sol.answer1())
    print(sol.answer2())
    
