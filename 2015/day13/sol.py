import re
from collections import defaultdict
from itertools import permutations

class Solution:
    def __init__(self, data):
        data = data.split("\n")[:-1]
        self.scores = defaultdict(dict)
        for info in data:
            a, b = re.findall(r"[A-Z]\w*", info)
            s = int(re.search(r"\d+", info).group(0))
            if "lose" in info:
                s = -s
            self.scores[a][b] = s

    def score(self, people):
        s = 0
        for i, p in enumerate(people):
            s += self.scores.get(p,{}).get(people[i-1],0)
            s += self.scores.get(p,{}).get(people[(i+1)%len(people)], 0)
        return s
            
    def answer1(self):
        s = 0
        people = list(self.scores.keys())
        alice = people.pop()
        for order in permutations(people):
            s = max(s, self.score((alice,)+order))
        return s

    def answer2(self):
        s = 0
        people = list(self.scores.keys())
        for order in permutations(people):
            s = max(s, self.score(("me",)+order))
        return s
        
test_data = """Alice would gain 54 happiness units by sitting next to Bob.
Alice would lose 79 happiness units by sitting next to Carol.
Alice would lose 2 happiness units by sitting next to David.
Bob would gain 83 happiness units by sitting next to Alice.
Bob would lose 7 happiness units by sitting next to Carol.
Bob would lose 63 happiness units by sitting next to David.
Carol would lose 62 happiness units by sitting next to Alice.
Carol would gain 60 happiness units by sitting next to Bob.
Carol would gain 55 happiness units by sitting next to David.
David would gain 46 happiness units by sitting next to Alice.
David would lose 7 happiness units by sitting next to Bob.
David would gain 41 happiness units by sitting next to Carol.
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    print(sol1.answer1())
    #assert(sol1.answer2() == ("c", 4))
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print(sol.answer1())
    print(sol.answer2())
    
