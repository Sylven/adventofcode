import re

class Light:
    def __init__(self):
        self.on = False
        self.light = 0

    def act(self, action):
        self.on = {"turn on": True, "turn off": False, "toggle": not self.on}[action]

    def act_bis(self, action):
        self.light += {"turn on": 1, "turn off": -1, "toggle": 2}[action]
        self.light = max(0, self.light)

class Solution:
    def __init__(self, data):
        self.data = data.split("\n")[:-1]
        self.lights = {(x, y) : Light() for x in range(1000) for y in range(1000)}

    def answer1(self):
        for instruct in self.data:
            action = re.match(r"[\w ]+(?= \d)", instruct).group(0)
            x_min, y_min, x_max, y_max = [int(i) for i in re.findall(r"\d+", instruct)]
            for x in range(x_min, x_max + 1):
                for y in range(y_min, y_max + 1):
                    self.lights[(x,y)].act(action)

        return len([l for l in self.lights.values() if l.on])

    def answer2(self):
        
        for instruct in self.data:
            action = re.match(r"[\w ]+(?= \d)", instruct).group(0)
            x_min, y_min, x_max, y_max = [int(i) for i in re.findall(r"\d+", instruct)]
            for x in range(x_min, x_max + 1):
                for y in range(y_min, y_max + 1):
                    self.lights[(x,y)].act_bis(action)

        return sum(l.light for l in self.lights.values())
        
test_data = """(())
"""

if __name__ == "__main__":
    #sol1 = Solution(test_data)
    #print(sol1.answer1())
    #assert(sol1.answer2() == ("c", 4))
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    #print(sol.answer1())
    print(sol.answer2())
    
