import hashlib

class Solution:
    def __init__(self, data):
        self.data = data

    def answer1(self):
        n = 0
        s = ""
        while s[:5] != "0"*5:
            n+=1
            s = hashlib.md5((self.data + str(n)).encode("utf-8")).hexdigest()
        return n

    def answer2(self):
        n = 0
        s = ""
        while s[:6] != "0"*6:
            n+=1
            s = hashlib.md5((self.data + str(n)).encode("utf-8")).hexdigest()
        return n
        
test_data = """abcdef"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    print(sol1.answer1())
    #assert(sol1.answer2() == ("c", 4))
    print("tests Ok !")
    print()

    data = "iwrupvqb"

    sol = Solution(data)
    print(sol.answer1())
    print(sol.answer2())
    
