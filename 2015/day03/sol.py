class Santa:
    def __init__(self):
        self.pos = (0,0)

    def move(self, vect):
        self.pos = (self.pos[0] + vect[0], self.pos[1] + vect[1])

class Solution:
    def __init__(self, data):
        self.data = data.split("\n")[0]

    def answer1(self):
        S = Santa()
        viewed = {S.pos}
        for c in self.data:
            S.move({"<":(-1,0), ">":(1,0), "^": (0,-1), "v": (0,1)}[c])
            viewed.add(S.pos)
        return len(viewed)

    def answer2(self):
        S = [Santa(), Santa()]
        viewed = {S[0].pos}
        i = 0
        for c in self.data:
            S[i].move({"<":(-1,0), ">":(1,0), "^": (0,-1), "v": (0,1)}[c])
            viewed.add(S[i].pos)
            i = (i+1)%2
        return len(viewed)
        
test_data = """<><><>
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    print(sol1.answer1())
    print(sol1.answer2())
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print(sol.answer1())
    print(sol.answer2())
    
