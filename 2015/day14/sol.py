import re

class Reindeer:
    def __init__(self, speed, duration, rest_time):
        self.speed = speed
        self.duration = duration
        self.resting_time = rest_time

        self.state = "run"
        self.weariness = 0
        self.rest = 0
        self.score = 0
        self.d = 0

    def distance(self, time):
        if time <= 0:
            return 0
        if time < self.duration:
            return time * self.speed
        return self.speed * self.duration + self.distance(time - self.duration - self.resting_time)

    def next_tick(self):
        if self.state == "run":
            self.d += self.speed
            self.weariness += 1
            if self.weariness == self.duration:
                self.state = "rest"
                self.weariness = 0
        else:
            self.rest += 1
            if self.rest == self.resting_time:
                self.rest = 0
                self.state = "run"

class Solution:
    def __init__(self, data):
        data = data.split("\n")[:-1]
        self.deers = {}
        for info in data:
            name = re.match(r"\w+", info).group(0)
            self.deers[name] = Reindeer(*map(int, re.findall(r"\d+", info)))

    def answer1(self, max_time = 2503):
        print(max(self.deers, key=lambda w: self.deers[w].distance(max_time)))
        return max(d.distance(max_time) for d in self.deers.values())        

    def answer2(self, max_time = 2503):
        for _ in range(max_time):
            for d in self.deers.values():
                d.next_tick()
            best = max(d.d for d in self.deers.values())
            for d in self.deers.values():
                if d.d == best:
                    d.score += 1
        print(max(self.deers, key=lambda w: self.deers[w].score))
        return max(d.score for d in self.deers.values())

        
test_data = """Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.
Dancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds.
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    print(sol1.answer1(1000))
    print(sol1.answer2(1000))
    #assert(sol1.answer2() == ("c", 4))
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print(sol.answer1())
    print(sol.answer2())
    
