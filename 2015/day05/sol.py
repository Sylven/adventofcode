import re

class Solution:
    def __init__(self, data):
        self.data = data.split("\n")[:-1]

    def is_nice(self, word):
        for w in ["ab", "cd", "pq", "xy"]:
            if w in word:
                return False
        if sum(word.count(c) for c in "aeiou") < 3:
            return False
        for i, c in enumerate(word[:-1]):
            if word[i+1] == c:
                return True
        return False

    def is_nice_2(self, word):
        if re.search(r"(..).*\1", word) == None:
            return False
        if re.search(r"(.).\1", word) == None:
            return False
        return True
        
    def answer1(self):
        return len([w for w in self.data if self.is_nice(w)])

    def answer2(self):
        return len([w for w in self.data if self.is_nice_2(w)])
        
test_data = """ugknbfddgicrmopn
jchzalrnumimnmhp
haegwjzuvuyypxyu
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    print(sol1.answer1())
    #assert(sol1.answer2() == ("c", 4))
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print(sol.answer1())
    print(sol.answer2())
    
