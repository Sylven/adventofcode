class Solution:
    def __init__(self, data):
        self.data = data.split("\n")[:-1]

    def answer1(self):
        nb = 0
        for row in self.data:
            nb += len(row) - len(eval(row))
        return nb

    def answer2(self):
        nb = 0
        for row in self.data:
            nb += length(row) - len(row)
        return nb

def length(word):
    n = 2
    for c in word:
        if c in ["'", '"', "\\"]:
            n += 2
        else:
            n += 1
    return n

test_data = """(())
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    #print(sol1.answer1())
    #assert(sol1.answer2() == ("c", 4))
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print(sol.answer1())
    print(sol.answer2())
    
