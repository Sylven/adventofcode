import re
import sys
sys.setrecursionlimit(100000)

class Solution:
    def __init__(self, data):
        self.data = data

    def next_sentence(self, word):
        res = ""
        while word != "":
            w = re.match(r"(.)\1*", word).group(0)
            res += str(len(w)) + w[0]
            word = word[len(w):]
        return res
        
    def answer1(self, c=40):
        w = self.data
        print(w)
        for _ in range(c):
            w = self.next_sentence(w)
            #print(w)
        return len(w)
        

    def answer2(self):
        return self.answer1(50)
        
test_data = """1"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    print(sol1.answer1(5))
    print(sol1.answer2())
    print("tests Ok !")
    print()

    data = "3113322113"

    sol = Solution(data)
    print(sol.answer1())
    print(sol.answer2())
    
