class Solution:
    def __init__(self, data):
        self.data = data
        self.strdata = str(data)
        self.scores = [3,7]
        self.pos1 = 0
        self.pos2 = 1
        self.pos_data = 0

    def compute_next_scores(self):
        s = self.scores[self.pos1]
        ss = self.scores[self.pos2]
        new = [int(i) for i in str(s+ss)]
        self.scores += new
        k = len(self.scores)
        self.pos1 = (self.pos1 + s + 1) % k
        self.pos2 = (self.pos2 + ss + 1) % k

    def compute_next_scores_2(self):
        s = self.scores[self.pos1]
        ss = self.scores[self.pos2]
        new = [int(i) for i in str(s+ss)]
        self.scores += new
        for c, cc in zip(self.strdata[self.pos_data:], new):
            if int(c) == cc:
                self.pos_data += 1
            else:
                self.pos_data = 0
        k = len(self.scores)
        self.pos1 = (self.pos1 + s + 1) % k
        self.pos2 = (self.pos2 + ss + 1) % k
        
    def answer1(self):
        while len(self.scores) <= self.data + 10:
            self.compute_next_scores()
        return int("".join(str(i) for i in self.scores[self.data:self.data+10]))

    def answer2(self):
        word = self.strdata
        sentense = "".join(str(i) for i in self.scores)
        if word in sentense:
            return sentense.index(word)
        
        while self.pos_data < len(word):
            self.compute_next_scores_2()
        return "".join(str(i) for i in self.scores).index(word)
        
        
test_data = 9
test_data2 = 51589
test_data3 = 01245

if __name__ == "__main__":
    sol1 = Solution(test_data)
    assert(sol1.answer1() == 5158916779)
    sol1 = Solution(test_data2)
    print(sol1.answer2())
    assert(sol1.answer2() == 9)
    print("tests Ok !")
    print()

    data = 503761
    sol = Solution(data)
    print(sol.answer1())
    print(sol.answer2())
    
