import pdb

class EndOfGameException(Exception):
    def __init__(self):
        pass

class Solution:
    def __init__(self, data, elve_att = 3):
        self.data = data.split("\n")[:-1]
        data = self.data
        self.grid = []
        self.troops = set()
        for y, row in enumerate(data):
            self.grid.append(list(row))
            #print(row)
            for x, elm in enumerate(row):
                if elm in ["G", "E"]:
                    t = Troop(x, y, elm, elve_att)
                    self.grid[-1][x] = t
                    self.troops.add(t)

    def next_round(self):
        for t in sorted(self.troops, key=lambda tt:(tt.y, tt.x)):
            self.take_turn_troop(t)
        dead = set()
        for t in self.troops:
            if t.dead:
                dead.add(t)
        while dead:
            t = dead.pop()
            self.troops.remove(t)

    def take_turn_troop(self, t):
        if t.dead:
            return
        x, y = t.x, t.y
        targets = {tt for tt in self.troops if tt.Type != t.Type and not tt.dead}
        if not targets:
            raise EndOfGameException
        in_range = set()
        for tt in targets:
            xx, yy = tt.x, tt.y
            if self.grid[yy-1][xx] in (".", t):
                dist, cell = self.path((x, y), (xx, yy-1))
                in_range.add((dist, (xx, yy-1), cell))
            if self.grid[yy][xx-1] in (".", t):
                dist, cell = self.path((x, y), (xx-1, yy))
                in_range.add((dist, (xx-1, yy), cell))
            if self.grid[yy][xx+1] in (".", t):
                dist, cell = self.path((x, y), (xx+1, yy))
                in_range.add((dist, (xx+1, yy), cell))
            if self.grid[yy+1][xx] in (".", t):
                dist, cell = self.path((x, y), (xx, yy+1))
                in_range.add((dist, (xx, yy+1), cell))
        reachable = list(filter(lambda tu: tu[0] >= 0, in_range))
        if reachable and not (0, None) in reachable:
            chosen = min(reachable, key=lambda tu: (tu[0],tu[1][1],tu[1][0]))
            chosen = chosen[2]
        else:
            chosen = None
        self.move(t, chosen)
        self.attack_phase(t)

    def move(self, t, pos):
        if pos == None:
            return
        x, y = t.x, t.y
        t.move(pos)
        xx, yy = pos
        self.grid[yy][xx] = t
        self.grid[y][x] = "."

    def attack_phase(self, t):
        x, y = t.x, t.y
        target = set()
        for (xx, yy) in [(x, y-1), (x-1, y), (x+1, y), (x, y+1)]:
            if 0 <= yy < len(self.grid) and 0 <= xx < len(self.grid[yy]):
                tt = self.grid[yy][xx]
                if isinstance(tt, Troop) and tt.Type != t.Type:
                    target.add(tt)
        if not target:
            return
        tt = min(target, key=lambda tt: (tt.hp, tt.y, tt.x))
        t.attack(tt)
        if tt.dead:
            self.grid[tt.y][tt.x] = "."

    def path(self, pos, ppos):
        distance = 0
        x_p, y_p = ppos
        done = {ppos}
        todo = {(x_p+1, y_p), (x_p-1, y_p), (x_p, y_p-1), (x_p, y_p+1)}
        while (not pos in done) and todo:
            distance += 1
            temp = set()
            while todo:
                x, y = todo.pop()
                if (x, y) in done or self.grid[y][x] != ".":
                    if (x, y) == pos:
                        done.add((x, y))
                    continue
                done.add((x, y))
                temp.update([(x, y+1), (x, y-1), (x+1, y), (x-1, y)])
            todo = temp
        ret = None    
        if pos in done:
            x, y = pos
            if (x, y-1) in done:
                ret = (x, y-1)
            elif (x-1, y) in done:
                ret = (x-1, y)
            elif (x+1, y) in done:
                ret = (x+1, y)
            elif (x, y+1) in done:
                ret = (x, y+1)
            return (distance, ret)
        return (-1, ret)
                        
    def answer1(self):
        turn = 0
        while True:
            turn += 1
            try:
                self.next_round()
            except EndOfGameException:
                res = 0
                team = []
                for row in self.grid:
                    print("".join(str(s) for s in row))
                for t in self.troops:
                    if not t.dead:
                        res += t.hp
                        team.append(t.Type)
                return res * (turn-1), team
            # print(turn)
            # for row in self.grid:
            #     print("".join(str(s) for s in row))
            # for t in self.troops:
            #     print(t.Type, (t.x, t.y), t.hp)
            t = self.troops.pop()
            self.troops.add(t)

    def answer2(self):
        nb_elves = len([t for t in self.troops if t.Type == "E"])
        print(nb_elves)
        a = 3
        c = 3
        b = None
        while b == None or b-a > 1:
            if b == None:
                c = c + 10
            else:
                c = (a+b)//2
            data = ""
            for row in self.data:
                data += "".join(str(i) for i in row) + "\n"
            Sol = Solution(data, c)
            res, team = Sol.answer1()
            print(c, team)
            if team[0] == "E" and len(team) == nb_elves:
                b = c
            else:
                a = c
        return c

class Troop:
    global_Id = 0
    
    def __init__(self, x, y, Type, att):
        self.x = x
        self.y = y
        self.Type = Type
        self.Id = Troop.global_Id
        Troop.global_Id += 1
        if Type == "G":
            self.power = 3
        else:
            self.power = att
        self.hp = 200
        self.dead = False

    def __hash__(self):
        return hash(self.Id)

    def __repr__(self):
        return self.Type

    def move(self, pos):
        self.x = pos[0]
        self.y = pos[1]

    def attack(self, other):
        if other == None:
            return
        other.hp -= self.power
        if other.hp <= 0:
            other.dead = True
            
    
        
test_data = ["""#######
#.G...#
#...EG#
#.#.#G#
#..G#E#
#.....#
#######
""",
             """#######
#G..#E#
#E#E.E#
#G.##.#
#...#E#
#...E.#
#######
""",
             """#######
#E..EG#
#.#G.E#
#E.##E#
#G..#.#
#..E#.#
#######      
""",
             """#######
#E.G#.#
#.#G..#
#G.#.G#
#G..#.#
#...E.#
#######
""",
             """#######
#.E...#
#.#..G#
#.###.#
#E#G#G#
#...#G#
#######
""",
             """#########
#G......#
#.E.#...#
#..##..G#
#...##..#
#...#...#
#.G...G.#
#.....G.#
#########
"""]

if __name__ == "__main__":
    #for data in test_data:
    #    sol1 = Solution(data)
    #    print(sol1.answer1())
    #assert(sol1.answer1() == 10)
    #assert(sol1.answer2() == ("c", 4))
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data, 40)
    print(sol.answer1())
    #print(sol.answer2())
    
