import matplotlib.pyplot as plt
import time

class Solution:
    def __init__(self, data):
        data = data.split("\n")[:-1]
        self.points = set()
        self.xmax = 0
        self.ymax = 0
        for info in data:
            i, j = info.index("<"), info.index(">")
            pos = eval(info[i+1:j])
            vi, vj = info.index("<", j+1), info.index(">", j+1)
            velocity = eval(info[vi+1:vj])
            self.points.add(Point(pos, velocity))
            #self.xmax = max(self.xmax, pos[0])
            #self.ymax = max(self.ymax, pos[1])

    def answer1(self):
        plt.ion()
        #plt.axis([0, self.xmax, 0, self.ymax])
        p = self.points.pop()
        p2 = self.points.pop()
        t = 0
        d = p.dist(p2)
        for t2 in range(100000):
            p.update(1)
            p2.update(1)
            d2 = p.dist(p2)
            if d2 < d:
                t = t2
            d = d2
        print(t)
        p.update(-100000)
        p2.update(-100000)
        self.points.add(p)
        self.points.add(p2)

        # for p in self.points:
        #     p.update(t-5)
        # for i in range(10):
        #     print(t+i)
        #     for p in self.points:
        #         p.plot()
        #         p.update(1)
        #     plt.pause(2)
        #     plt.clf()
        for p in self.points:
            p.update(10144)

            p.plot()
        plt.pause(200)
        plt.close()

    def answer2(self):
        ...
        

class Point:
    def __init__(self, pos, velocity):
        self.x = pos[0]
        self.y = -pos[1]

        self.vx = velocity[0]
        self.vy = -velocity[1]

    def update(self, step):
        self.x += self.vx * step
        self.y += self.vy * step

    def plot(self):
        plt.plot(self.x, self.y, 'ro', markersize=22)

    def dist(self, other):
        return (self.x - other.x)**2 + (self.y - other.y)**2
        
test_data = """position=< 9,  1> velocity=< 0,  2>
position=< 7,  0> velocity=<-1,  0>
position=< 3, -2> velocity=<-1,  1>
position=< 6, 10> velocity=<-2, -1>
position=< 2, -4> velocity=< 2,  2>
position=<-6, 10> velocity=< 2, -2>
position=< 1,  8> velocity=< 1, -1>
position=< 1,  7> velocity=< 1,  0>
position=<-3, 11> velocity=< 1, -2>
position=< 7,  6> velocity=<-1, -1>
position=<-2,  3> velocity=< 1,  0>
position=<-4,  3> velocity=< 2,  0>
position=<10, -3> velocity=<-1,  1>
position=< 5, 11> velocity=< 1, -2>
position=< 4,  7> velocity=< 0, -1>
position=< 8, -2> velocity=< 0,  1>
position=<15,  0> velocity=<-2,  0>
position=< 1,  6> velocity=< 1,  0>
position=< 8,  9> velocity=< 0, -1>
position=< 3,  3> velocity=<-1,  1>
position=< 0,  5> velocity=< 0, -1>
position=<-2,  2> velocity=< 2,  0>
position=< 5, -2> velocity=< 1,  2>
position=< 1,  4> velocity=< 2,  1>
position=<-2,  7> velocity=< 2, -2>
position=< 3,  6> velocity=<-1, -1>
position=< 5,  0> velocity=< 1,  0>
position=<-6,  0> velocity=< 2,  0>
position=< 5,  9> velocity=< 1, -2>
position=<14,  7> velocity=<-2,  0>
position=<-3,  6> velocity=< 2, -1>
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    #sol1.answer1()
    #assert(sol1.answer2() == ("c", 4))
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    sol.answer1()
    #print(sol.answer2())
    
