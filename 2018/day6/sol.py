from collections import defaultdict

class Solution:
    def __init__(self, data):
        self.points = set()
        min_x = max_x = min_y = max_y = None
        for point in data.split('\n'):
            if point == "":
                continue
            x, y = point.split()
            x = int(x[:-1])
            y = int(y)
            if min_x == None:
                min_x = max_x = x
                min_y = max_y = y
            min_x = min(x, min_x)
            max_x = max(x, max_x)
            min_y = min(y, min_y)
            max_y = max(y, max_y)
            self.points.add(Point(x, y))
        self.grid = Grid(min_x, max_x, min_y, max_y, self.points)
        self.grid.actualise_distances()

    def answer1(self):
        infinite = self.grid.get_infite_points()
        area = defaultdict(int)
        for row in self.grid.grid:
            for p in row:
                pp = p.closest_point
                if pp and not pp in infinite:
                    area[pp] += 1
        return max(area.values())

    def answer2(self, boundary):
        nb = 0
        for row in self.grid.grid:
            for p in row:
                dist = 0
                for pp in self.points:
                    dist += p.dist(pp)
                if dist < boundary:
                    nb += 1
        return nb
        

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.closest_point = None

    def dist(self, other):
        return abs(self.x - other.x) + abs(self.y - other.y)
        
    def __repr__(self):
        return '(' + str(self.x) + ", " + str(self.y) + ')'

    def __hash__(self):
        return self.__repr__().__hash__()

class Grid:
    def __init__(self, min_x, max_x, min_y, max_y, points):
        self.grid = [[Point(x, y) for x in range(min_x, max_x+1)] for y in range(min_y, max_y+1)]
        self.points = points.copy()

    def actualise_distances(self):
        for row in self.grid:
            for point in row:
                d = min(point.dist(p) for p in self.points)
                P = [p for p in self.points if point.dist(p) == d]
                if len(P) == 1:
                    point.closest_point = P[0]

    def get_infite_points(self):
        res = set()
        for p in self.grid[0] + self.grid[-1]:
            if p.closest_point:
                res.add(p.closest_point)
        for row in self.grid[1:-1]:
            for p in (row[0], row[-1]):
                if p.closest_point:
                    res.add(p.closest_point)
        return res

    
        
test_data = """1, 1
1, 6
8, 3
3, 4
5, 5
8, 9
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    assert(sol1.answer1() == 17)
    assert(sol1.answer2(32) == 16)
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print("first answer : ", end="")
    print(sol.answer1())
    print("second answer : ", end="")
    print(sol.answer2(10000))
    
