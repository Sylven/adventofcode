def addr(a, b, c, reg):
    reg[c] = reg[a] + reg[b]

def addi(a, b, c, reg):
    reg[c] = reg[a] + b

def mulr(a, b, c, reg):
    reg[c] = reg[a] * reg[b]

def muli(a, b, c, reg):
    reg[c] = reg[a] * b

def banr(a, b, c, reg):
    reg[c] = reg[a] & reg[b]

def bani(a, b, c, reg):
    reg[c] = reg[a] & b

def borr(a, b, c, reg):
    reg[c] = reg[a] | reg[b]

def bori(a, b, c, reg):
    reg[c] = reg[a] | b

def setr(a, b, c, reg):
    reg[c] = reg[a]

def seti(a, b, c, reg):
    reg[c] = a

def gtir(a, b, c, reg):
    reg[c] = int(a > reg[b])

def gtri(a, b, c, reg):
    reg[c] = int(reg[a] > b)

def gtrr(a, b, c, reg):
    reg[c] = int(reg[a] > reg[b])

def eqir(a, b, c, reg):
    reg[c] = int(a == reg[b])

def eqri(a, b, c, reg):
    reg[c] = int(reg[a] == b)

def eqrr(a, b, c, reg):
    reg[c] = int(reg[a] == reg[b])

class Solution:
    def __init__(self, data):
        data = data.split("\n")[:-1]
        self.data = []
        self.ip = 0
        self.bound_register = int(data[0][-1])
        self.reg = [0]*6
        for info in data[1:]:
            inst, a, b, c = info.split()
            inst = eval(inst)
            a, b, c = int(a), int(b), int(c)
            self.data.append((inst, a, b, c))

    def execute(self):
        inst, a, b, c = self.data[self.ip]
        self.reg[self.bound_register] = self.ip
        inst(a, b, c, self.reg)
        self.ip = self.reg[self.bound_register] + 1
        if self.ip >= len(self.data):
            self.ip = -1

    def answer1(self):
        while self.ip >= 0:
            if self.ip == 28:
                return self.reg[4]
            self.execute()
        return self.reg[0]

    def answer2(self):
        seen = set()
        val = 0
        prev = None
        # while self.ip >= 0:
        #     if self.ip == 28:
        #         prev = val
        #         val = self.foo(val)
        #         print(self.reg, prev, val)
        #         if tuple(self.reg) in seen:
        #             return self.reg
        #         seen.add(tuple(self.reg))
        #     self.execute()
        while not val in seen:
            seen.add(val)
            prev = val
            val = self.foo(val)
        return prev

    def foo(self, value):
        k = value | 2**16
        val = 4332021
        while True:
            val += k & 255
            val = val & (2**24-1)
            val *= 65899
            val = val & (2**24-1)
            if k < 256:
                return val
            k = (k // 256)
   
if __name__ == "__main__":
    #sol1 = Solution(test_data)
    #print(sol1.answer1())
    #print(sol1.answer2())
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print(sol.answer1())
    print(sol.answer2())
    
