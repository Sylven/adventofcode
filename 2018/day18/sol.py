import time
from collections import defaultdict

class Cell:
    def __init__(self, data):
        self.content = data
        self.adjacents = {".": 0, "#": 0, "|": 0}

    def __repr__(self):
        return self.content

    def update(self):
        if self.content == "." and self.adjacents["|"] >= 3:
            self.content = "|"
        elif self.content == "|" and self.adjacents["#"] >= 3:
            self.content = "#"
        elif self.content == "#":
            if self.adjacents["#"] == 0 or self.adjacents["|"] == 0:
                self.content = "."
        self.adjacents = {".": 0, "#": 0, "|": 0}

class Solution:
    def __init__(self, data):
        data = data.split("\n")[:-1]
        self.grid = []
        self.minute = 0
        for row in data:
            self.grid.append([Cell(c) for c in row])

    def __repr__(self):
        w = ""
        for row in self.grid:
            w += "".join(c.__repr__() for c in row) + "\n"
        return w[:-1]

    def update_adjacents(self):
        for i, row in enumerate(self.grid):
            for j, c in enumerate(row):
                for ii in range(max(0, i-1), min(len(self.grid),i+2)):
                    for jj in range(max(0, j-1), min(len(self.grid[i]),j+2)):
                        if ii != i or jj != j:
                            c.adjacents[self.grid[ii][jj].content] += 1

    def next_tick(self):
        self.update_adjacents()
        self.minute += 1
        for row in self.grid:
            for c in row:
                c.update()

    def ressource_value(self):
        w = self.__repr__()
        return w.count("#") * w.count("|")

    def answer1(self):
        print(self)
        for _ in range(10):
            self.next_tick()
            print()
            time.sleep(0.2)
            print(self)
        return self.ressource_value()

    def answer2(self):
        print(self)
        values = defaultdict(set)
        while self.minute < 1000000000:
            self.next_tick()
            if self.minute > 1500:
                values[self.minute % 28].add(self.ressource_value())
                print(values)
        return self.ressource_value()
        
test_data = """.#.#...|#.
.....#|##|
.|..|...#.
..|#.....#
#.#|||#|#|
...#.||...
.|....|...
||...#|.#|
|.||||..|.
...#.|..|.
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    print(sol1.answer1())
    #assert(sol1.answer2() == ("c", 4))
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print(sol.answer1())
    print(sol.answer2())
    
