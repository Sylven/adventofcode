class Solution:
    def __init__(self, data):
        self.data = data
        self.grid = Grid(data)

    def answer1(self):
        return self.grid.get_best_fuel()[1:]

    def answer2(self):
        return max(self.grid.fuel_sum, key=self.grid.fuel_sum.get)
        
class Grid:
    def __init__(self, Id):
        self.Id = Id
        self.grid = [[Cell(x, y, Id) for x in range(1,301)] for y in range(1, 301)]
        self.fuel_sum = {}
        self.fuel_sum_row = {}
        self.fuel_sum_col = {}
        self.compute_fuel_sums()


    def compute_fuel_sums(self):
        for k in range(1, 301):
            print(k)
            self.compute_fuel(k)

    def compute_fuel(self, k):
        for x in range(300-k):
            for y in range(300-k):
                power = self.grid[y][x].power
                if k > 1:
                    self.fuel_sum[(x+1, y+1, k)] = self.fuel_sum[(x+2, y+2, k-1)] + self.fuel_sum_row[(x+2, y+1, k-1)] + self.fuel_sum_col[(x+1, y+2, k-1)] + power
                    self.fuel_sum_row[(x+1, y+1, k)] = self.fuel_sum_row[(x+2, y+1, k-1)] + power
                    self.fuel_sum_col[(x+1, y+1, k)] = self.fuel_sum_col[(x+1, y+2, k-1)] + power
                else:
                    self.fuel_sum[(x+1, y+1, k)] = power
                    self.fuel_sum_row[(x+1, y+1, k)] = power
                    self.fuel_sum_col[(x+1, y+1, k)] = power
        
    def get_best_fuel(self, k=3):
        best_score = -10
        best_x = best_y = None
        for y in range(300-k):
            for x in range(300-k):
                score = sum(self.grid[j][i].power for j in range(y, y+k) for i in range(x, x+k))
                if score > best_score:
                    best_score = score
                    best_x = x+1
                    best_y = y+1
        return (best_score, best_x, best_y)
        

class Cell:
    def __init__(self, x, y, Id):
        self.x = x
        self.y = y
        self.set_fuel(Id)

    def set_fuel(self, Id):
        rack_Id = self.x + 10
        power = rack_Id * self.y
        power += Id
        power *= rack_Id
        power = (power // 100) % 10
        power -= 5
        self.power = power
    
test_data = 18

if __name__ == "__main__":
    # sol1 = Solution(test_data)
    # print(sol1.answer1())
    # assert(sol1.answer1() == (33,45))
    # print(sol1.answer2())
    # assert(sol1.answer2() == (90,269,16))
    print("tests Ok !")
    print()

    data = 6548

    sol = Solution(data)
    print(sol.answer1())
    print(sol.answer2())
    
