def addr(a, b, c, reg):
    reg[c] = reg[a] + reg[b]

def addi(a, b, c, reg):
    reg[c] = reg[a] + b

def mulr(a, b, c, reg):
    reg[c] = reg[a] * reg[b]

def muli(a, b, c, reg):
    reg[c] = reg[a] * b

def banr(a, b, c, reg):
    reg[c] = reg[a] & reg[b]

def bani(a, b, c, reg):
    reg[c] = reg[a] & b

def borr(a, b, c, reg):
    reg[c] = reg[a] | reg[b]

def bori(a, b, c, reg):
    reg[c] = reg[a] | b

def setr(a, b, c, reg):
    reg[c] = reg[a]

def seti(a, b, c, reg):
    reg[c] = a

def gtir(a, b, c, reg):
    reg[c] = int(a > reg[b])

def gtri(a, b, c, reg):
    reg[c] = int(reg[a] > b)

def gtrr(a, b, c, reg):
    reg[c] = int(reg[a] > reg[b])

def eqir(a, b, c, reg):
    reg[c] = int(a == reg[b])

def eqri(a, b, c, reg):
    reg[c] = int(reg[a] == b)

def eqrr(a, b, c, reg):
    reg[c] = int(reg[a] == reg[b])

class Solution:
    def __init__(self, data):
        data = data.split("\n")[:-1]
        self.data = []
        self.ip = 0
        self.bound_register = int(data[0][-1])
        self.reg = [0]*6
        for info in data[1:]:
            inst, a, b, c = info.split()
            inst = eval(inst)
            a, b, c = int(a), int(b), int(c)
            self.data.append((inst, a, b, c))
        print(len(self.data))

    def execute(self):
        inst, a, b, c = self.data[self.ip]
        self.reg[self.bound_register] = self.ip
        inst(a, b, c, self.reg)
        self.ip = self.reg[self.bound_register] + 1
        if self.ip >= len(self.data):
            self.ip = -1

    def answer1(self):
        while self.ip >= 0:
            self.execute()
            #print(self.reg, self.ip)
        return self.reg[0]

    def answer2(self):
        self.reg = [0,1,10551432,1,0,0]
        self.foo(self.reg)
        print(self.reg)
        self.ip = 16
        c = 0
        while self.ip >= 0:
            self.execute()
            print(self.reg, self.ip)
            c += 1
        return self.reg[0]
        

    def bla(self, reg):
        while reg[1] <= reg[2]:
            reg[4] = reg[3]*reg[1]
            if reg[4] == reg[2]:
                reg[0] += reg[3]
            reg[1] += 1

    def foo(self, reg):
        while reg[3] <= reg[2]:
            self.reg[1] = 1
            self.bla2(reg)
            self.reg[3] += 1
        

    def bla2(self, reg):
        if reg[2] % reg[3] == 0:
            if reg[2] // reg[3] >= reg[1]:
                reg[0] += reg[3]
        reg[1] += 1
            
        
test_data = """#ip 0
seti 5 0 1
seti 6 0 2
addi 0 1 0
addr 1 2 3
setr 1 0 0
seti 8 0 4
seti 9 0 5
"""

if __name__ == "__main__":
    #sol1 = Solution(test_data)
    #print(sol1.answer1())
    #print(sol1.answer2())
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    #print(sol.answer1())
    print(sol.answer2())
    
