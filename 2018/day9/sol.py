class Solution:
    def __init__(self, data):
        s = data.split()
        self.players = int(s[0])
        self.max_marble = int(s[-2])
        self.game = Game(self.players)

    def answer1(self):
        for _ in range(self.max_marble):
            self.game.compute_next()
        return self.game.get_high_score()

    def answer2(self):
        ...

class Game:
    def __init__(self, players):
        self.nb_players = players
        self.scores = {i : 0 for i in range(players)}
        self.state = Node(0)
        self.next_marble = 1

    def compute_next(self):
        marble = self.next_marble
        self.next_marble += 1
        if marble % 23 > 0:
            self.state = self.state.get_right()
            self.state.add_right(Node(marble))
            self.state = self.state.get_right()
        else:
            for _ in range(6):
                self.state = self.state.get_left()
            value = self.state.remove_left()
            self.scores[marble % self.nb_players] += marble + value

    def get_high_score(self):
        return max(self.scores.values())

    def __repr__(self):
        w = ""
        temp = self.state
        while self.state.get_right() != temp:
            temp = temp.get_left()
            w += temp.__repr__() + " "
        return w + self.state.__repr__()
            

class Node:
    def __init__(self, value):
        self.value = value
        self.next_node = self
        self.previous_node = self

    def get_right(self):
        return self.next_node

    def get_left(self):
        return self.previous_node

    def add_right(self, other):
        other.next_node = self.next_node
        other.previous_node = self
        self.next_node.previous_node = other
        self.next_node = other

    def add_left(self, other):
        other.previous_node = self.previous_node
        other.next_node = self
        self.previous_node = other

    def remove_left(self):
        node = self.previous_node
        self.previous_node = node.previous_node
        self.previous_node.next_node = self
        return node.value

    def __repr__(self):
        return str(self.value)
    
    

test_data = ["""9 players; last marble is worth 25 points""",
             """10 players; last marble is worth 1618 points""",
             """13 players; last marble is worth 7999 points""",
             """17 players; last marble is worth 1104 points""",
             """21 players; last marble is worth 6111 points""",
             """30 players; last marble is worth 5807 points"""]

answers = [32, 8317, 146373, 2764, 54718, 37305]

validation = "441 players; last marble is worth 7103200 points"


if __name__ == "__main__":
    sols_test = [Solution(data) for data in test_data]
    for sol, answer in zip(sols_test, answers):
        my_ans = sol.answer1()
        assert(my_ans == answer)
    #assert(sol1.answer1() == 10)
    #assert(sol1.answer2() == ("c", 4))
    print("tests Ok !")
    print()

    data = validation

    sol = Solution(data)
    print(sol.answer1())
    #print(sol.answer2())
    
