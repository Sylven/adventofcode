class Solution:
    def __init__(self, data):
        self.data = data.split("\n")[:-1]
        self.doable = set()
        self.pieces = {}
        for s in self.data:
            info = s.split()
            restr = info[1]
            other = info[7]
            if not restr in self.pieces:
                self.pieces[restr] = Piece(restr)
                self.doable.add(restr)
            if not other in self.pieces:
                self.pieces[other] = Piece(other)
            if other in self.doable:
                self.doable.remove(other)
            restr = self.pieces[restr]
            other = self.pieces[other]
            restr.add_next(other)
            

    def answer1(self):
        doable = self.doable.copy()
        res = ""
        while doable:
            p = min(doable)
            res += p
            doable.remove(p)
            piece = self.pieces[p]
            new_pieces = piece.actualise()
            doable.update(new_pieces)
        return res

    ## Warning question 2 needs that question 1 is not done before
    def answer2(self, nb_workers):
        free_workers = set(Worker() for _ in range(nb_workers))
        occupied_workers = set()
        time = 0
        while self.doable or occupied_workers:
            while self.doable and free_workers:
                p = min(self.doable)
                w = free_workers.pop()
                self.doable.remove(p)
                piece = self.pieces[p]
                w.task = piece
                w.time = piece.time
                occupied_workers.add(w)
            next_time = min(w.time for w in occupied_workers)
            time += next_time
            finished = set()
            for w in occupied_workers:
                p = w.update(next_time)
                if p:
                    finished.add(w)
                    new_pieces = p.actualise()
                    self.doable.update(new_pieces)
            free_workers.update(finished)
            occupied_workers.difference_update(finished)
        return time

class Piece:
    def __init__(self, name):
        self.name = name
        self.next_piece = set()
        self.restriction = set()
        self.time = ord(self.name) - ord("A") + 61

    def add_next(self, other):
        self.next_piece.add(other)
        other.restriction.add(self)

    def actualise(self):
        res = set()
        for p in self.next_piece:
            p.restriction.remove(self)
            if not p.restriction:
                res.add(p.name)
        return res

class Worker:
    def __init__(self):
        self.task = None
        self.time = None

    def update(self, time):
        self.time -= time
        if self.time == 0:
            return self.task


        
test_data = """Step C must be finished before step A can begin.
Step C must be finished before step F can begin.
Step A must be finished before step B can begin.
Step A must be finished before step D can begin.
Step B must be finished before step E can begin.
Step D must be finished before step E can begin.
Step F must be finished before step E can begin.
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    assert(sol1.answer1() == "CABDFE")
    sol1 = Solution(test_data)
    print(sol1.answer2(2))
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    #print(sol.answer1())
    print(sol.answer2(5))
    
