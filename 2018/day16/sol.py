def addr(a, b, c, reg):
    reg[c] = reg[a] + reg[b]

def addi(a, b, c, reg):
    reg[c] = reg[a] + b

def mulr(a, b, c, reg):
    reg[c] = reg[a] * reg[b]

def muli(a, b, c, reg):
    reg[c] = reg[a] * b

def banr(a, b, c, reg):
    reg[c] = reg[a] & reg[b]

def bani(a, b, c, reg):
    reg[c] = reg[a] & b

def borr(a, b, c, reg):
    reg[c] = reg[a] | reg[b]

def bori(a, b, c, reg):
    reg[c] = reg[a] | b

def setr(a, b, c, reg):
    reg[c] = reg[a]

def seti(a, b, c, reg):
    reg[c] = a

def gtir(a, b, c, reg):
    reg[c] = int(a > reg[b])

def gtri(a, b, c, reg):
    reg[c] = int(reg[a] > b)

def gtrr(a, b, c, reg):
    reg[c] = int(reg[a] > reg[b])

def eqir(a, b, c, reg):
    reg[c] = int(a == reg[b])

def eqri(a, b, c, reg):
    reg[c] = int(reg[a] == b)

def eqrr(a, b, c, reg):
    reg[c] = int(reg[a] == reg[b])

oper = {addr, addi, mulr, muli, banr, bani, borr, bori, setr, seti, gtir, gtri, gtrr, eqri, eqir, eqrr}

class Solution:
    def __init__(self, data, test):
        self.data = []
        self.test = test.split("\n")[:-1]
        data = data.split("\n")
        for be, inst, af, _ in zip(*[iter(data)]*4):
            self.data.append((be, inst, af))
        print(len(self.data))
        

    def answer1(self):
        res = 0
        for be, inst, af in self.data:
            beL = eval(be[8:])
            instL = [int(i) for i in inst.split()]
            afL = eval(af[8:])
            nb = 0
            for o in oper:
                cop = beL.copy()
                o(*instL[1:], cop)
                if cop == afL:
                    nb += 1
            if nb >= 3:
                res += 1
        return res

    def answer2(self):
        opcode = {i : oper.copy() for i in range(16)}
        for be, inst, af in self.data:
            beL = eval(be[8:])
            instL = [int(i) for i in inst.split()]
            code = instL[0]
            afL = eval(af[8:])
            op = set()
            for o in opcode[code]:
                cop = beL.copy()
                o(*instL[1:], cop)
                if cop == afL:
                    op.add(o)
            opcode[code].intersection_update(op)
            assert(len(opcode[code]) > 0)
            if len(opcode[code]) == 1:
                o = opcode[code].pop()
                for S in opcode.values():
                    if o in S:
                        S.remove(o)
                opcode[code].add(o)
        assert(all(len(opcode[code])==1 for code in range(16)))

        for key, value in opcode.items():
            opcode[key] = value.pop()

        reg = [0]*4
        for inst in self.test:
            code, *inst = [int(i) for i in inst.split()]
            opcode[code](*inst, reg)
        return reg[0]
            
        
test_data = """Before: [3, 2, 1, 1]
9 2 1 2
After:  [3, 2, 2, 1]
"""

if __name__ == "__main__":
    #sol1 = Solution(test_data)
    #print(sol1.answer1())
    #assert(sol1.answer2() == ("c", 4))
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()
    with open("input_2.txt", "r") as my_file:
        test = my_file.read()

    sol = Solution(data, test)
    print(sol.answer1())
    print(sol.answer2())
    
