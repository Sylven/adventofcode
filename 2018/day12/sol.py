class Solution:
    def __init__(self, data):
        data = data.split("\n")[:-1]
        self.state = data[0].split()[2]
        self.current_state = 0
        self.rules = set()
        for rule in data[2:]:
            pattern, output = rule.split(" => ")
            if output == "#":
                self.rules.add(pattern)
        self.first_index = [0]

    def next_state(self):
        previous = "...." + self.state + "...."
        res = ""
        for i in range(2, len(previous)-2):
            if previous[i-2:i+3] in self.rules:
                res += "#"
            else:
                res += "."
        self.current_state += 1
        self.first_index.append(self.first_index[-1] -2 + res.index("#"))
        self.state = res.strip(".")

    def answer1(self):
        while self.current_state < 20:
            self.next_state()
        return sum(i+self.first_index[20] for i, c in enumerate(self.state) if c == "#")

    def answer2(self):
        k = 5000
        while self.current_state < k:
            self.next_state()
        return sum(i+self.first_index[k] for i, c in enumerate(self.state) if c == "#")


        
test_data = """initial state: #..#.#..##......###...###

...## => #
..#.. => #
.#... => #
.#.#. => #
.#.## => #
.##.. => #
.#### => #
#.#.# => #
#.### => #
##.#. => #
##.## => #
###.. => #
###.# => #
####. => #
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    print(sol1.answer1())
    assert(sol1.answer1() == 325)
    #assert(sol1.answer2() == ("c", 4))
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print(sol.answer1())
    print(sol.answer2())
    
