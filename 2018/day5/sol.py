class Solution:
    def __init__(self, data):
        self.data = data

    def answer1(self):
        prev = self.data
        temp = reduction(self.data)
        while len(temp) < len(prev):
            prev = temp
            temp = reduction(prev)
        return len(temp)

    def answer2(self):
        letters = set(self.data.lower())
        sol = {}
        for c in letters:
            new_data = self.data.replace(c, "").replace(c.upper(), "")
            sol_c = Solution(new_data)
            sol[c] = sol_c.answer1()
        return min(sol.items(), key=lambda t:t[1])
        

def reduction(w):
    res = ""
    last = None
    for c in w:
        if last == None:
            last = c
            continue
        if (c.islower() and c.upper()==last) or (c.isupper() and c.lower()==last):
            last = None
        else:
            res += last
            last = c
    if last:
        res += last
    return res
        
test_data = "dabAcCaCBAcCcaDA"
test = "uaAubBBBadDdaaAADAZ"

if __name__ == "__main__":
    sol1 = Solution(test_data)
    assert(sol1.answer1() == 10)
    assert(sol1.answer2() == ("c", 4))
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.readline()[:-1]

    sol = Solution(data)
    print(sol.answer1())
    print(sol.answer2())
    
