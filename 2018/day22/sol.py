import sys
sys.setrecursionlimit(10000)

class Cell:
    Id = 0
    
    def __init__(self, pos):
        self.lvl = None
        self.pos = pos
        self.Type = None
        self.Id = Cell.Id
        Cell.Id += 1

    def __repr__(self):
        return self.Type

    def __hash__(self):
        return hash(self.Id)

    def risk(self):
        return [".", "=", "|"].index(self.Type)

class Cell_bis:
    Id = 0
    def __init__(self, cell=None, Type=None, pos=None):
        self.Id = Cell_bis.Id
        Cell_bis.Id += 1
        
        if cell != None:
            self.Type = cell.Type
            self.pos = cell.pos
            self.equip = {".": "T", "=" : "G", "|" : "N"}[cell.Type]
            self.change = Cell_bis(Type=self.Type, pos=self.pos)
            self.change.change = self
        else:
            self.Type = Type
            self.pos = pos
            self.equip = {".": "G", "=" : "N", "|" : "T"}[Type]
        self.dist = None
        self.dist_bis = None
        self.up = None
        self.down = None
        self.right = None
        self.left = None

    def __hash__(self):
        return self.Id

class Solution:
    def __init__(self, data):
        data = data.split("\n")
        self.depth = int(data[0].split()[1])
        self.target = data[1].split()[1].split(",")
        self.target = (int(self.target[0]), int(self.target[1]))
        xx, yy = self.target
        
        self.grid = {(x,y):Cell((x,y)) for x in range(xx+yy+1) for y in range(yy+xx+1)}
        self.compute_type((xx, yy))
        self.compute_type((0,0))

    def __repr__(self):
        w = ""
        for y in range(self.target[1]+1):
            for x in range(self.target[0]+1):
                w += self.grid[(x, y)].__repr__()
            w += "\n"
        return w[:-1]

    def compute_type(self, pos):
        x, y = pos
        cell = self.grid[pos]
        if cell.Type:
            return
        if pos == (0,0):
            cell.index = 0
            cell.Type = "."
            return
        if pos[0] == 0 or pos[1] == 0:
            cell.lvl = (pos[0]*16807 + pos[1]*48271 + self.depth) % 20183
        else:
            self.compute_type((x-1, y))
            self.compute_type((x, y-1))
            index = self.grid[(x-1,y)].lvl * self.grid[(x, y-1)].lvl
            cell.lvl = ((index + self.depth) % 20183)
        cell.Type = [".", "=", "|"][cell.lvl % 3]
        if pos == self.target:
            cell.index = 0
            cell.Type = "."

    def create_cell_bis(self):
        self.torch_cell = {}
        self.gear_cell = {}
        self.neither_cell = {}
        for (x, y), c in self.grid.items():
            cell = Cell_bis(c)
            cell_2 = cell.change
            S = {"T": self.torch_cell, "G":self.gear_cell, "N":self.neither_cell}
            S[cell.equip][(x,y)] = cell
            S[cell_2.equip][(x,y)] = cell_2

    def connect_cells(self):
        for d in [self.torch_cell, self.gear_cell, self.neither_cell]:
            print(len(d))
            for (x, y), cell in d.items():
                cell.left = d.get((x-1,y), None)
                cell.right = d.get((x+1, y), None)
                cell.up = d.get((x, y-1), None)
                cell.down = d.get((x, y+1), None)
            
    def compute_distances(self):
        cell = self.torch_cell[(0,0)]
        cell.dist = 0
        to_treat = {cell}
        n = 0
        while to_treat:
            if n % 1000 == 0:
                print(n)
            cell = min(to_treat, key=lambda c: c.dist)
            to_treat.remove(cell)
            if cell.pos == self.target and cell.equip == 'T':
                return cell.dist
            for c in [cell.up, cell.down, cell.right, cell.left]:
                if c:
                    if c.dist == None:
                        c.dist = cell.dist + 1
                        to_treat.add(c)
                    else:
                        c.dist = min(c.dist, cell.dist+1)
            c = cell.change
            if c.dist == None:
                c.dist = cell.dist + 7
                to_treat.add(c)
            else:
                c.dist = min(c.dist, cell.dist+7)
            n+=1

    def answer1(self):
        xx, yy = self.target
        return sum(c.risk() for (x, y), c in self.grid.items() if x <= xx and y <= yy)

    def answer2(self):
        self.compute_type(max(self.grid))
        self.create_cell_bis()
        self.connect_cells()
        return self.compute_distances()
        
        
        
test_data = """depth: 510
target: 10,10"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    print(sol1.answer1())
    print(sol1.answer2())
    #assert(sol1.answer2() == ("c", 4))
    print("tests Ok !")
    print()

    data = """depth: 8112
target: 13,743
"""

    sol = Solution(data)
    print(sol.answer1())
    print(sol.answer2())
    
