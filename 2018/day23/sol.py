import pdb

class Point:
    def __init__(self, pos):
        self.x, self.y, self.z = pos

    def dist(self, other):
        dist = abs(self.x - other.x) + abs(self.y - other.y) + abs(self.z - other.z)
        return dist

    def __hash__(self):
        return hash(self.__repr__())

    def __repr__(self):
        return str((self.x, self.y, self.z))

class Nanobot(Point):
    def __init__(self, pos, r):
        Point.__init__(self, pos)
        self.r = r

    def in_range(self, other):
        return self.dist(other) <= self.r

    def intersect(self, other):
        return self.dist(other) <= self.r + other.r
        

class Interval:
    def __init__(self, a, b):
        if a > b:
            raise ValueError("Interval must have lower bound as first value")
        self.down = a
        self.up = b

    def __eq__(self, other):
        if not isinstance(other, Interval):
            return False
        return self.down == other.down and self.up == other.up

    def __repr__(self):
        return str([self.down, self.up])

    def __hash__(self):
        return hash(self.__repr__())

    def intersection(self, other):
        a = max(self.down, other.down)
        b = min(self.up, other.up)
        if a > b:
            return None
        return Interval(a, b)
    
class Entity:
    def __init__(self, bot=None):
        self.xyz = None
        self.xyzz = None
        self.xyyz = None
        self.xyyzz = None
        self.nb_bots = 0
        if bot != None:
            x, y, z, r = bot.z, bot.y, bot.z, bot.r
            self.xyz = Interval(x+y+z-r, x+y+z+r)
            self.xyzz = Interval(x+y-z-r, x+y-z+r)
            self.xyyz = Interval(x-y+z-r, x-y+z+r)
            self.xyyzz = Interval(x-y-z-r, x-y-z+r)
            self.nb_bots = 1
            
    def __eq__(self, other):
        if not isinstance(other, Entity):
            return False
        return self.xyz == other.xyz and self.xyzz == other.xyzz and self.xyyz == other.xyyz and self.xyyzz == other.xyyzz

    def __repr__(self):
        return str([i.__repr__() for i in [self.xyz, self.xyzz, self.xyyz, self.xyyzz]])

    def __hash__(self):
        return hash(self.__repr__())

    def intersection(self, other):
        e = Entity()
        e.xyz = self.xyz.intersection(other.xyz)
        e.xyzz = self.xyzz.intersection(other.xyzz)
        e.xyyz = self.xyyz.intersection(other.xyyz)
        e.xyyzz = self.xyyzz.intersection(other.xyyzz)
        e.nb_bots = self.nb_bots + other.nb_bots # works if not any bots in common
        if None in [e.xyz, e.xyzz, e.xyyz, e.xyyzz]:
            return None
        return e
            
class Cell:
    def __init__(self, bot, ent):
        self.bot = bot
        self.ent = ent
        self.sons = []

    def add_son(self, other):
        e = self.ent.intersection(other.ent)
        if e != None and e != self.ent:
            for s in self.sons:
                s.add_son(other)
            self.sons.append(Cell(other.bot, e))

class Solution:
    def __init__(self, data):
        data = data.split("\n")[:-1]
        self.bots = []
        for info in data:
            i, j = info.index("<"), info.index(">")
            k = info.index("=", j)
            pos = eval(info[i+1:j])
            r = int(info[k+1:])
            self.bots.append(Nanobot(pos, r))
            
    def answer1(self):
        bot = max(self.bots, key=lambda b: b.r)
        nb = 0
        for b in self.bots:
            if bot.in_range(b):
                nb += 1
        return nb

    def get_max_intersect(self, S, tot_set, i, best):
        res = []
        l = 0
        while len(S) + len(tot_set) >= best and S:
            if i == 0:
                print("############# len(S) #############", len(S))
            b = S.pop()
            SS = self.in_range[b].intersection(tot_set)
            if SS == tot_set:
                S_temp = S.intersection(self.in_range[b])
                if len(S_temp) < l:
                    continue
                tot_temp = tot_set.union({b})
                R = self.get_max_intersect(S_temp, tot_temp, i+1, best)
                if R:
                    if len(R[0]) >= l:
                        if len(R[0]) > l:
                            res = R
                        else:
                            res += R
                        l = len(R[0])
                        best = len(tot_set) + l + 1
                        for s in R:
                            s.add(b)
                elif l == 0:
                    res.append({b})
                    best = 1 + len(tot_set)
        return res
    
    def answer2(self):
        self.in_range = {}
        for b in self.bots:
            self.in_range[b] = set()
            for bb in self.bots:
                if b.intersect(bb):
                    self.in_range[b].add(bb)
        print('bla')
        #print(self.in_range)
        res = self.get_max_intersect(set(self.bots), set(), 0, 0)
        print("blu")
        print(len(res))
        P = Point((0,0,0))
        for S in res:
            n = 0
            for b in S:
                n = max(n, max(0, P.dist(b)-b.r))
            print(n)
        return n

test_data = """pos=<0,0,0>, r=4
pos=<1,0,0>, r=1
pos=<4,0,0>, r=3
pos=<0,2,0>, r=1
pos=<0,5,0>, r=3
pos=<0,0,3>, r=1
pos=<1,1,1>, r=1
pos=<1,1,2>, r=1
pos=<1,3,1>, r=1
"""

test_data_2 = """pos=<10,12,12>, r=2
pos=<12,14,12>, r=2
pos=<16,12,12>, r=4
pos=<14,14,14>, r=6
pos=<50,50,50>, r=200
pos=<10,10,10>, r=5
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    print(sol1.answer1())
    print(sol1.answer2())
    sol1 = Solution(test_data_2)
    print(sol1.answer2())
    #assert(sol1.answer1() == 10)
    #assert(sol1.answer2() == ("c", 4))
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print(sol.answer1())
    print(sol.answer2())
    
