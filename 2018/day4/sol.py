from collections import defaultdict

class Guard:
    def __init__(self, Id):
        self.Id = Id
        self.minutes_asleep = defaultdict(int)

    def update(self, date_asleep, date_wakes_up):
        minute_asleep = int(date_asleep.split(":")[1][:-1])
        minute_wakes_up = int(date_wakes_up.split(":")[1][:-1])
        for i in range(minute_asleep, minute_wakes_up):
            self.minutes_asleep[i] += 1

    def time_asleep(self):
        return sum(self.minutes_asleep.values())

    def most_time_per_minute(self):
        if self.minutes_asleep:
            return max(self.minutes_asleep.values())
        else:
            return 0

    def most_frequent_minute(self):
        if self.minutes_asleep:
            return max(self.minutes_asleep, key=lambda i: self.minutes_asleep[i])

    
if __name__ == "__main__":
    with open("input.txt", "r") as my_file:
        data = my_file.readlines()
    data.sort()
    guards = {}
    Id = 0
    last_asleep = ""
    for entry in data:
        info = entry.split()
        date, info = " ".join(info[:2]), info[2:]
        print(date, info)
        if info[0] == "Guard" and info[2] == "begins":
            Id = int(info[1][1:])
            if not Id in guards:
                guards[Id] = Guard(Id)
        elif info[0] == "falls":
            last_asleep = date
        else:
            guards[Id].update(last_asleep, date)

    #Id = max(guards, key=lambda g : guards[g].time_asleep())
    Id = max(guards, key=lambda g : guards[g].most_time_per_minute())
    g = guards[Id]
    print(Id * g.most_frequent_minute())
                
