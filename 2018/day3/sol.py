class Claim:
    def __init__(self, Id, coord, size):
        self.Id = Id
        pos = coord.split(",")
        self.col = int(pos[0])
        self.row = int(pos[1])
        dim = size.split('x')
        self.length = int(dim[0])
        self.hight = int(dim[1])

    def positions(self):
        pos = set()
        for i in range(self.hight):
            for j in range(self.length):
                pos.add((self.row + i, self.col+j))
        return pos

    def __hash__(self):
        return self.Id.__hash__()

    def __repr__(self):
        return self.Id
                    
if __name__ == "__main__":
    data = []
    with open("input.txt", 'r') as my_file:
        for line in my_file:
            info = line.split()
            Id = info[0][1:]
            coord = info[2][:-1]
            size = info[-1]
            data.append(Claim(Id, coord, size))
    positions = set()
    no_intersections = set()
    for claim in data:
        pos = claim.positions()
        if not positions.intersection(pos):
            no_intersections.add(claim)
        positions.update(pos)

    positions = set()
    no_intersections_bis = set()
    for claim in data[::-1]:
        pos = claim.positions()
        if not positions.intersection(pos):
            no_intersections_bis.add(claim)
        positions.update(pos)

    
    
    print(no_intersections_bis.intersection(no_intersections))
