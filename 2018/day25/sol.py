CONST_DIST = 3

class Point:
    def __init__(self, pos):
        self.pos = tuple(pos)

    def __repr__(self):
        return self.pos.__repr__()

    def __hash__(self):
        return hash(self.__repr__())

    # Suppose that self and other are of the same dimention
    def dist(self, other):
        return sum(abs(a-b) for a, b in zip(self.pos, other.pos))

class Constallation:
    def __init__(self, p):
        self.stars = {p}

    def possible_add(self, po):
        return any(p.dist(po) <= CONST_DIST for p in self.stars)

    def merge(self, other):
        self.stars.update(other.stars)
        
class Solution:
    def __init__(self, data):
        data = data.split("\n")[:-1]
        self.constallations = []
        for p in data:
            P = Point(map(int, p.split(",")))
            C = None
            to_remove = []
            for cc in self.constallations:
                if cc.possible_add(P):
                    if C == None:
                        C = cc
                        cc.stars.add(P)
                    else:
                        C.merge(cc)
                        to_remove.append(cc)
            if C == None:
                self.constallations.append(Constallation(P))
            for cc in to_remove:
                self.constallations.remove(cc)

    def answer1(self):
        return len(self.constallations)

    def answer2(self):
        ...
        
        
test_data = ["""0,0,0,0
3,0,0,0
0,3,0,0
0,0,3,0
0,0,0,3
0,0,0,6
9,0,0,0
12,0,0,0
""",
             """-1,2,2,0
0,0,2,-2
0,0,0,-2
-1,2,0,0
-2,-2,-2,2
3,0,2,-1
-1,3,2,2
-1,0,-1,0
0,2,1,-2
3,0,0,0
""",
             """1,-1,0,1
2,0,-1,0
3,2,-1,0
0,0,3,1
0,0,-1,-1
2,3,-2,0
-2,2,0,0
2,-2,0,-1
1,-1,0,-1
3,2,0,2""",
             """1,-1,-1,-2
-2,-2,0,1
0,2,1,3
-2,3,-2,1
0,2,3,-2
-1,-1,1,-2
0,-2,-1,0
-2,2,3,-1
1,2,2,0
-1,-2,0,-2"""]

validators = [2, 4, 3, 8]

if __name__ == "__main__":
    for data, valid in zip(test_data, validators):
        sol1 = Solution(data)
        assert(sol1.answer1() == valid)
    #assert(sol1.answer1() == 10)
    #assert(sol1.answer2() == ("c", 4))
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()[:-1]

    sol = Solution(data)
    print(sol.answer1())
    #print(sol.answer2())
    
