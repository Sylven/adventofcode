import sys
sys.setrecursionlimit(10000)

opposite = {"N":"S", "S":"N", "E":"W", "W":"E"}

class Cell:
    def __init__(self, word, cell=None, direction=None):
        self.adj = {"N":None, "S":None, "E":None, "W":None}
        if direction:
            self.adj[direction] = cell
            cell.adj[opposite[direction]] = self
        self.dist = None

        self.words = {word}

    def __repr__(self):
        if self.dist == None:
            return "  X"
        return "%3d"%self.dist

    def compute_dist(self, dist):
        if self.dist != None and self.dist <= dist:
            return
        self.dist = dist
        dist += 1
        for cell in self.adj.values():
            if cell != None:
                cell.compute_dist(dist)

class Solution:
    def __init__(self, data):
        self.data = data[:-1]
        self.maze = {(0,0) : Cell(self.data)}
        self.create_maze((0,0), self.data[1:])

    def __repr__(self):
        x_min = min(x for (x,y) in self.maze)
        x_max = max(x for (x,y) in self.maze)
        y_min = min(y for (x,y) in self.maze)
        y_max = max(y for (x,y) in self.maze)
        w = ""
        for y in range(x_min, x_max+1):
            for x in range(y_min, y_max+1):
                if (x, y) in self.maze:
                    w += self.maze[(x, y)].__repr__()
                else:
                    w += "#"
            w += "\n"
        return w[:-1]

    def create_maze(self, pos, w):
        x, y = pos
        current = self.maze[pos]
        for i, c in enumerate(w):
            if c == "$":
                return
            elif c == "(":
                break
            elif c == "E":
                x += 1
            elif c == "N":
                y -= 1
            elif c == "W":
                x -= 1
            elif c == "S":
                y += 1
            opp = opposite[c]
            if not (x, y) in self.maze:
                self.maze[(x, y)] = Cell(w[i+1:], current, opp)
            else:
                #print(x, y)
                current.adj[c] = self.maze[(x, y)]
                self.maze[(x, y)].adj[opp] = current
                if w[i+1:] in self.maze[(x,y)].words:
                    return
                self.maze[(x, y)].words.add(w[i+1:])
            current = self.maze[(x, y)]
        level = 0
        words = []
        temp = ""
        i += 1
        while level > 0 or w[i] != ")":
            if w[i] == "|" and level == 0:
                words.append(temp)
                temp = ""
                i+=1
                continue
            temp += w[i]
            if w[i] == "(":
                level += 1
            elif w[i] == ")":
                level -= 1
            i+=1
        words.append(temp)
        for ww in words:
            self.create_maze((x,y), ww+w[i+1:])
            
    def compute_dist(self):
        self.maze[(0,0)].compute_dist(0)
            
    def answer1(self):
        #print(self.data)
        self.compute_dist()
        #print(self)
        for cell in self.maze.values():
            if cell.dist == None:
                print(cell.adj)
        return max(cell.dist for cell in self.maze.values())

    def answer2(self):
        nb = 0
        for cell in self.maze.values():
            if cell.dist >= 1000:
                nb += 1
        return nb
        
        
test_data = ["""^WNE$
""",
             """^ENWWW(NEEE|SSE(EE|N))$
""",
             """^ENNWSWW(NEWS|)SSSEEN(WNSE|)EE(SWEN|)NNN$
""",
             """^ESSWWN(E|NNENN(EESS(WNSE|)SSS|WWWSSSSE(SW|NNNE)))$
""",
             """^WSSEESWWWNW(S|NENNEEEENN(ESSSSW(NWSW|SSEN)|WSWWN(E|WWS(E|SS))))$
"""]

verif1 = [3, 10, 18, 23, 31]
if __name__ == "__main__":
    for i, test in enumerate(test_data):
        sol1 = Solution(test)
        assert(sol1.answer1() == verif1[i])
    #assert(sol1.answer2() == ("c", 4))
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print(sol.answer1())
    print(sol.answer2())
    
