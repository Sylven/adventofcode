class Solution:
    def __init__(self, data):
        data = data.split("\n")[:-1]
        self.grid = []
        self.charts = []
        for y, row in enumerate(data):
            self.grid.append(list(row))
            #print(row)
            for x, c in enumerate(row):
                if c in {"<", ">", "^", "v"}:
                    self.grid[y][x] = {"<":"-", ">":"-", "^":"|", "v":"|"}[c]
                    self.charts.append(Chart(x, y, {"<":3, ">":1, "^":0, "v":2}[c]))

    def next_tick(self):
        self.charts.sort(key=lambda c:(c.y, c.x))
        positions = set()
        colisions = set()
        colision_crossed = []
        for i, c in enumerate(self.charts):
            x, y = c.move()
            rail = self.grid[y][x]
            c.update_orient(rail)
            for cc in self.charts[i+1:]:
                if cc.x == x and cc.y == y:
                    colisions.add((x, y))
                    colision_crossed.append(cc)
                    continue
            if (x, y) in positions:
                colisions.add((x, y))
            else:
                positions.add((x, y))
        for c in colision_crossed:
            colisions.add((c.x, c.y))
        return colisions

    def next_tick_remove(self):
        to_remove = []
        self.charts.sort(key=lambda c:(c.y, c.x))
        for c in self.charts:
            if c.dead == True:
                continue
            x, y = c.move()
            rail = self.grid[y][x]
            c.update_orient(rail)
            for cc in self.charts:
                if cc != c and cc.x == c.x and cc.y == c.y:
                    if c.dead == True or cc.dead == True:
                        print("bla")
                    c.dead = True
                    cc.dead = True

        for c in self.charts:
            if c.dead:
                to_remove.append(c)
        for c in to_remove:
            self.charts.remove(c)
        
                

    def answer1(self):
        k = 0
        colision = False
        while not colision:
            k+=1
            colision = self.next_tick()
        return min(colision, key=lambda u: (u[1], u[0]))

    def answer2(self):
        k = 0
        while len(self.charts) > 1:
            self.next_tick_remove()
        return self.charts[0].x, self.charts[0].y


class Chart:
    intersection = [-1, 0, 1]
    def __init__(self, x, y, direction):
        """
          - direction : 0 -> up
                        1 -> right
                        2 -> down
                        3 -> left
        """
        self.x = x
        self.y = y
        self.orient = direction
        self.next_intersection = 0
        self.dead = False

    def move(self):
        self.x += {1:1, 3:-1}.get(self.orient, 0)
        self.y += {0:-1, 2:1}.get(self.orient, 0)
        return self.x, self.y

    def update_orient(self, c):
        if c == "+":
            self.orient = (self.orient + Chart.intersection[self.next_intersection]) % 4
            self.next_intersection = (self.next_intersection + 1) % 3
        elif c == "/":
            self.orient = {0: 1, 1: 0, 2: 3, 3: 2}[self.orient]
        elif c == "\\":
            self.orient = {0: 3, 1: 2, 2: 1, 3: 0}[self.orient]
        
test_data = """/->-\\
|   |  /----\\
| /-+--+-\\  |
| | |  | v  |
\\-+-/  \\-+--/
  \\------/   
"""

test_data2 = """/>-<\\  
|   |  
| /<+-\\
| | | v
\\>+</ |
  |   ^
  \\<->/
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    assert(sol1.answer1() == (7,3))
    sol1 = Solution(test_data2)
    assert(sol1.answer2() == (6,4))
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    #print(sol.answer1())
    print(sol.answer2())
    
