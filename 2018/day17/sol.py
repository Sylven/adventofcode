import matplotlib.pyplot as plt

class Water:
    def __init__(self):
        self.still = False

class Solution:
    def __init__(self, data):
        data = data.split("\n")[:-1]
        self.grid = {(500, 0) : "+"}
        self.sources = {(500, 0)}
        self.x_min = 500
        self.x_max = 500
        self.y_min = None
        self.y_max = 0
        for info in data:
            s, v = info.split()
            mi, ma = v.split("..")
            for i in range(int(mi[2:]), int(ma)+1):
                j = int(s[2:-1])
                if s[0] == "x":
                    x, y = j, i
                else:
                    x, y = i, j
                self.grid[(x, y)] = "#"
                if self.y_min == None:
                    self.y_min = y
                self.x_min = min(self.x_min, x)
                self.x_max = max(self.x_max, x)
                self.y_max = max(self.y_max, y)
                self.y_min = min(self.y_min, y)

    def __repr__(self):
        r = ""
        for y in range(174):
            w = ""
            for x in range(self.x_min-1, self.x_max+2):
                w += self.grid.get((x, y), ".")
            r += w + "\n"
        return r[:-1]

    def propagate_water(self):
        temp = set()
        while self.sources:
            x, y = self.sources.pop()
            if not (x, y+1) in self.grid:
                self.grid[(x, y+1)] = "|"
                temp.add((x, y+1))
                continue
            if self.grid[(x, y+1)] == "|":
                continue
            level, still, sources = self.level_water((x, y))
            c = "|~"[still]
            for pos in level:
                self.grid[pos] = c
            temp.update(sources)
            if still:
                temp.add((x, y-1))
        self.sources = temp

    def level_water(self, pos):
        sources = set()
        level, still= self._level_water_side(pos, 1)
        l, s = self._level_water_side(pos, -1)
        if not still:
            sources.add(max(level))
        if not s:
            sources.add(min(l))
        level.update(l)
        return level, still and s, sources
            
    def _level_water_side(self, pos, d):
        x, y = pos
        level = {pos}
        xx = x+d
        while True:
            if self.grid.get((xx, y), '.') == "#":
                return level, True
            level.add((xx, y))
            if self.grid.get((xx, y+1), ".") in ".|":
                return level, False
            xx += d
                
    def answer1(self):
        print(self.y_max)
        step = 0
        while True:
            step += 1
            self.propagate_water()
            print(self)
            to_remove = set()
            for x, y in self.sources:
                if y >= self.y_max:
                    to_remove.add((x, y))
            self.sources.difference_update(to_remove)
            if not self.sources:
                res = 0
                for (x, y), c in self.grid.items():
                    if y >= self.y_min and c in "~|":
                        res += 1
                return res

    def answer2(self):
        res = 0
        for c in self.grid.values():
            if c == "~":
                res += 1
        return res
        
test_data = """x=495, y=2..7
y=7, x=495..501
x=501, y=3..7
x=498, y=2..4
x=506, y=1..2
x=498, y=10..13
x=504, y=10..13
y=13, x=498..504
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    assert(sol1.answer1()==57)
    assert(sol1.answer2() == 29)
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print(sol.answer1())
    print(sol.answer2())
    
