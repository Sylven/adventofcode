class Solution:
    def __init__(self, data):
        self.data = data[:-1].split()[::-1]
        self.tree = Node(self.data)
        assert(self.data == [])
        
    def answer1(self):
        return self.tree.sum_data()

    def answer2(self):
        return self.tree.get_value()


class Node:
    def __init__(self, info):
        self.nb_childs = int(info.pop())
        self.nb_data = int(info.pop())
        self.childs = []
        self.data = []
        self.value = None
        for _ in range(self.nb_childs):
            self.childs.append(Node(info))
        for _ in range(self.nb_data):
            self.data.append(int(info.pop()))

    def sum_data(self):
        res = sum(self.data)
        for child in self.childs:
            res += child.sum_data()
        return res

    def get_value(self):
        if self.value != None:
            return self.value
        if self.nb_childs == 0:
            self.value = sum(self.data)
            return self.value
        res = 0
        for i in self.data:
            if i <= self.nb_childs:
                res += self.childs[i-1].get_value()
        self.value = res
        return res
        
test_data = """2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    assert(sol1.answer1() == 138)
    print(sol1.answer2())
    assert(sol1.answer2() == 66)
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print(sol.answer1())
    print(sol.answer2())
    
