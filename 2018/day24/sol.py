import re

VERBOSE = False

class Troop:
    def __init__(self, Id, info, boost=0):
        numbers = [int(i) for i in re.findall("\d+", info)]
        # print(info)
        self.units = numbers[0]
        self.hp = numbers[1]
        self.dmg = numbers[2] + boost
        self.init = numbers[3]
        self.Id = Id
        self.target = None
        self.dead = False

        self.dmg_type = re.search("(?<=\d )\w+(?= damage)", info).group(0)
        self.weak = set()
        self.immune = set()
        weak = re.search("(?<=weak to )[\w, ]+(?=[;)])", info)
        immune = re.search("(?<=immune to )[\w, ]+(?=[;)])", info)
        if weak:
            self.weak = set(weak.group(0).split(", "))
        if immune:
            self.immune = set(immune.group(0).split(", "))

    def __repr__(self):
        return 'group ' + str(self.Id)

    def power(self):
        return self.units * self.dmg

    def damage_dealt(self, other):
        if self.dmg_type in other.weak:
            return self.power() * 2
        if self.dmg_type in other.immune:
            return 0
        return self.power()
    
    def select_target(self, opp):
        assert(self.dead == False)
        self.target = None
        if opp == []:
            return None
        t = max(opp, key=lambda o: (self.damage_dealt(o), o.power(), o.init))
        assert(t.dead == False)
        if self.damage_dealt(t) > 0:
            self.target = t
        return self.target

    def attack(self):
        if self.dead or self.target == None:
            return
        assert(self.target.dead == False)
        damage = self.damage_dealt(self.target)
        loss = damage // self.target.hp
        self.target.units -= loss
        if self.target.units <= 0:
            self.target.dead = True

class Team:
    def __init__(self, Id, name, troops, boost=0):
        self.Id = Id
        self.name = name
        self.troops = []
        for i, t in enumerate(troops):
            self.troops.append(Troop(i, t, boost))
        self.alive = True
        if VERBOSE:
            print(self.name)
            for t in self.troops:
                print(t.immune, t.weak, t.dmg_type)
            print()

    def __repr__(self):
        return self.name

class Solution:
    def __init__(self, data, boost = 0):
        self.data = data
        data = data[:-1].split("\n\n")
        self.teams = []
        self.troops = []
        for i, group in enumerate(data):
            group = group.split("\n")
            if group[0] == "Immune System:":
                self.teams.append(Team(i, group[0][:-1], group[1:], boost))
            else:
                self.teams.append(Team(i, group[0][:-1], group[1:]))
            self.troops += self.teams[i].troops
        self.troops.sort(key=lambda t: t.init, reverse=True)

    def selection_phase(self):
        for i, T in enumerate(self.teams):
            #print(T)
            O = self.teams[(i+1)%2]
            opp_troops = O.troops.copy()
            for t in sorted(T.troops, key=lambda t: (t.power(), t.init), reverse=True):
                chosen = t.select_target(opp_troops)
                #print(t, "chose", chosen)
                if chosen:
                    opp_troops.remove(chosen)

    def attacking_phase(self):
        if all(t.target == None for t in self.troops):
            raise ValueError("Must treat this case !")
        for t in self.troops:
            t.attack()
        dead = []
        for T in self.teams:
            for t in T.troops:
                if t.dead:
                    dead.append((T.Id, t))
        for Id, t in dead:
            self.teams[Id].troops.remove(t)
            self.troops.remove(t)
        for T in self.teams:
            if T.troops == []:
                T.alive = False

    def answer1(self):
        i = 0
        while self.teams[0].alive and self.teams[1].alive:
            i+=1
            self.selection_phase()
            self.attacking_phase()
            if VERBOSE:
                print("####### round", i, "######")
                for T in self.teams:
                    print(T.name)
                    for t in T.troops:
                        print(t.units)
                    print()
        for T in self.teams:
            if T.alive:
                return (T.name, sum(t.units for t in T.troops))

    def answer2(self):
        a = 1
        b = None
        c = 1
        while b == None or b-a > 1:
            if b == None:
                c *= 2
            else:
                c = (b+a) // 2
            s = Solution(self.data, c)
            name, score = s.answer1()
            print(c, name, score)
            if name == "Infection":
                a = c+1
            else:
                b = c
        print(b)
        s = Solution(self.data, b)
        return s.answer1()
        
test_data = """Immune System:
17 units each with 5390 hit points (weak to radiation, bludgeoning) with an attack that does 4507 fire damage at initiative 2
989 units each with 1274 hit points (immune to fire; weak to bludgeoning, slashing) with an attack that does 25 slashing damage at initiative 3

Infection:
801 units each with 4706 hit points (weak to radiation) with an attack that does 116 bludgeoning damage at initiative 1
4485 units each with 2961 hit points (immune to radiation; weak to fire, cold) with an attack that does 12 slashing damage at initiative 4
"""

if __name__ == "__main__":
    sol1 = Solution(test_data)
    print(sol1.answer1())
    print(sol1.answer2())
    print("tests Ok !")
    print()

    with open("input.txt", "r") as my_file:
        data = my_file.read()

    sol = Solution(data)
    print(sol.answer1())
    sol = Solution(data, 35)
    print(sol.answer1())
    # print(sol.answer2())
    
